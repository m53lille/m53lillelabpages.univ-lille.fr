\documentclass[a4paper,12pt,reqno]{amsart}
\usepackage{lille}
\lilleset{
  % solutions=true,
  titre=\sisujet{Devoir surveillé}\sisolutions{Solutions du devoir surveillé},
  date=20 décembre 2024,
  duree=3 heures,
}
\input{m53}
% ---- Indication ------
\begin{document}
\sisujet{
  \tsvp
 % \vspace{4mm}
  \attention~
  \emph{Les documents et les objets électroniques sont interdits. Les exercices sont indépendants. Toutes les réponses doivent être justifiées.}
    \vspace{4mm}
}

% -----------------------------------------------
\begin{exo}\emph{(Séries de Fourier)}

  On considère la fonction $f$ de période $2\pi$, définie par $f(x)=\ch(x)$, $x\in [-\pi,\pi[$.
  \begin{enumerate}
    \item Dessiner le graphe de $f$.
    \item Déterminer pour $n\in\mathbb{Z}$, $c_n(f)$, puis pour $n\in\mathbb{N}$, $a_n(f)$ et $b_n(f)$.
    \item Étudier la convergence de la série de Fourier de $f$.
    \item En déduire les valeurs de $\sum_{n=1}^{+\infty}\frac{(-1)^n}{n^2+1}$ et $\sum_{n=1}^{+\infty}\frac{1}{n^2+1}$.
  \end{enumerate}
\end{exo}

\begin{solution}

  \begin{enumerate}
    \item ~\vskip -\baselineskip
      \includegraphics[width=.5\linewidth]{cosh2pi}
    \item En utilisant que $e^{\pm in\pi} = (-1)^n$, on a
    \begin{align*}
      c_n(f)
      &= \frac{1}{2\pi}\int_{-\pi}^{\pi} \ch(t)e^{-int} \dd t
      = \frac{1}{2\pi}\int_{-\pi}^{\pi} \frac{e^{t}+e^{-t}}{2}e^{-int} \dd t\\
      &= \frac{1}{4\pi}\int_{-\pi}^{\pi} e^{(1-in)t} + e^{(-1-in)t}\dd t
      = \frac{1}{4\pi}\left[\frac{e^{(1-in)t}}{1-in} + \frac{e^{(-1-in)t}}{-1-in}\right]_{-\pi}^{\pi}\\
      &= \frac{1}{4\pi}\left[\frac{e^{(1-in)\pi}}{1-in} + \frac{e^{(-1-in)\pi}}{-1-in} - \frac{e^{(1-in)(-\pi)}}{1-in} - \frac{e^{(-1-in)(-\pi)}}{-1-in}\right]\\
      &= \frac{(-1)^n}{4\pi}\left[\frac{e^{\pi}-e^{-\pi}}{1-in} - \frac{e^{\pi}-e^{-\pi}}{-1-in}\right]
      = \frac{\sh(\pi)}{\pi}\frac{(-1)^{n}}{1+n^2}.
    \end{align*}
    En utilisant que $\ch$ est paire, on constate aisément, comme on peut le voir sur le graphe, que $f$ est paire. Ainsi\footnote{On aurait pu également évoquer que $c_n = c_{-n}$ pour aboutir à la même conclusion.}, pour tout $n\in\mathbb{N}$,  $b_n(f)=0$ et $a_n = 2c_n(f) = \frac{2\sh(\pi)}{\pi}\frac{(-1)^{n}}{1+n^2}$.
    \item La fonction $f$ est continue et $C^1$ par morceaux. En effet $\ch$ est $C^1$ sur $[-\pi,\pi[$ et de plus comme $\ch(-\pi) = \ch(\pi)$, elle se prolonge, par périodicité $2\pi$, en une fonction continue sur $\mathbb{R}$ qui est $C^1$ sur $\mathbb{R}\setminus \left\{\pi+2\pi\mathbb{Z}\right\}$ \emph{(voir le graphe dans la première question)}. Par conséquent, selon le cours, la série de Fourier $Sf$ de $f$ converge vers $f$ normalement (donc uniformément) sur $\mathbb{R}$.
    \item En se basant sur les deux questions précédentes, on a, pour tout $x\in\mathbb{R}$,
    \[
      f(x) = \frac{\sh(\pi)}{\pi}+\frac{2\sh(\pi)}{\pi}\sum_{n=1}^{+\infty}\frac{(-1)^{n}}{1+n^2}\cos(nx).
    \]
    Pour $x=0$, on obtient
    \[
      1 = \frac{\sh(\pi)}{\pi}+\frac{2\sh(\pi)}{\pi}\sum_{n=1}^{+\infty}\frac{(-1)^{n}}{1+n^2},
    \]
    d'où
    \[
      \sum_{n=1}^{+\infty}\frac{(-1)^{n}}{1+n^2} = \frac{\pi}{2\sh(\pi)} - \frac{1}{2}.
    \]
    Et pour $x=\pi$, en utilisant que $\cos(n\pi) = (-1)^n$, on obtient
    \[
      \ch(\pi) = \frac{\sh(\pi)}{\pi}+\frac{2\sh(\pi)}{\pi}\sum_{n=1}^{+\infty}\frac{(-1)^{n}}{1+n^2}(-1)^n,
    \]
    d'où
    \[
      \sum_{n=1}^{+\infty}\frac{1}{1+n^2} = \frac{\pi\ch(\pi)}{2\sh(\pi)} - \frac{1}{2}.
    \]
  \end{enumerate}
\end{solution}


% -----------------------------------------------
\begin{exo}\emph{(Intégrales définies dépendant d'un paramètre)}

  Soit
  \[
    F(x)\coloneqq\frac{1}{\pi}\displaystyle\int_0^\pi \frac{t\sin(t)}{1-x\cos(t)}\dd t.
  \]
  \begin{enumerate}
    \item Montrer que $F$ est de classe $C^{1}$ sur $]-1,1[$. Exprimer $F'(x)$ sous forme d'une intégrale, sans chercher à la calculer.
    \item Montrer que, pour $x\in]-1,1[$,
    \[
      xF'(x)=-F(x)+\frac{1}{1+x}+\frac{1}{\pi}\int_0^\pi \frac{\cos(t)}{1-x\cos(t)}\dd t.
    \]
    \begin{indication}
      Effectuer une intégration par partie dans l'intégrale donnant $xF'(x)$.
    \end{indication}
    \item En faisant le changement de variable $u\coloneqq\tan(t/2)$, montrer que, pour $x\in]0,1[$,
    \[
      \frac{1}{\pi}\int_0^\pi \frac{\cos(t)}{1-x\cos(t)}\dd t=-\frac{1}{x}+\frac{1}{x\sqrt{1-x^2}}.
    \]
    \item En déduire que $F$ est solution sur $]0,1[$ de l'équation différentielle
    \[
      x y'+y=-\frac{1}{x}+\frac{1}{1+x}+\frac{1}{x\sqrt{1-x^2}},
    \]
    vérifiant la condition \emph{au bord} $y(0)=1$.
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item\label{2a} On pose $h(t,x) \coloneqq \frac{t\sin(t)}{1-x\cos(t)}$. La fonction $h$ est bien définie et continue sur $[0,\pi]\times]-1,1[$, puisque sur cet ensemble $\abs{x\cos(t)} < 1$ et donc $1-x\cos(t) \neq 0$. D'après le théorème sur la continuité des intégrales définies dépendants d'un paramètre, $F$ est continue sur $]-1,1[$.

    De plus $\frac{\partial}{\partial x}h(t,x) = \frac{t\sin(t)\cos(t)}{(1-x\cos(t))^2}$ est continue sur $[0,\pi]\times]-1,1[$, donc $F$ est de classe $C^1$ sur $]-1,1[$. En utilisant le théorème sur la dérivabilité des intégrales définies dépendants d'un paramètre, la fonction $F$ est dérivable sur $]-1,1[$ et pour tout $x\in]-1,1[$, on a
    \[
      F'(x) = \frac{1}{\pi}\int_0^\pi \frac{t\sin(t)\cos(t)}{(1-x\cos(t))^2} \dd t.
    \]
    \item\label{2b} En appliquant le résultat précédent, suivi d’une intégration par parties, on trouve
    \begin{align*}
      xF'(x)
      &= \frac{1}{\pi}\int_0^\pi t\cos(t)\frac{x\sin(t)}{(1-x\cos(t))^2} \dd t
      = \frac{1}{\pi}\int_0^\pi t\cos(t)\left(\frac{-1}{1-x\cos(t)}\right)' \dd t\\
      & = -\frac{1}{\pi}\left[t\cos(t)\frac{1}{1-x\cos(t)}\right]_0^\pi + \frac{1}{\pi}\int_0^\pi (\cos(t)-t\sin(t))\frac{1}{1-x\cos(t)} \dd t\\
      &= -\frac{1}{\pi}\left[\frac{\pi\cos(\pi)}{1-x\cos(\pi)} - 0\right] + \frac{1}{\pi}\int_0^\pi \frac{\cos(t)}{1-x\cos(t)} \dd t - \frac{1}{\pi}\int_0^\pi \frac{t\sin(t)}{1-x\cos(t)} \dd t\\
      &= \frac{1}{1+x}+\frac{1}{\pi}\int_0^\pi \frac{\cos(t)}{1-x\cos(t)}\dd t -F(x).
    \end{align*}
    \item\label{2c} En posant $u \coloneqq \tan(t/2)$, on a $\cos(t) = \frac{1-u^2}{1+u^2}$ et $\dd t = \frac{2\dd u}{1+u^2}$. Ainsi, pour $x\in]0,1[$, on a
    \begin{align*}
      \frac{1}{\pi}\int_0^\pi \frac{\cos(t)}{1-x\cos(t)}\dd t
      &= \frac{1}{\pi}\int_0^{+\infty} \frac{1-u^2}{(1+u^2)-x(1-u^2))}\frac{2\dd u}{1+u^2}\\
      &= \frac{2}{\pi}\int_0^{+\infty} \frac{1-u^2}{((1-x) + (1+x)u^2)(1+u^2)}\dd u.
    \end{align*}
    On cherche à décomposer en éléments simples la fraction rationnelle en $u^2$, pour $x \neq 0$, sous la forme
    \[
      \frac{1-u^2}{((1-x) + (1+x)u^2)(1+u^2)} = \frac{A}{1+u^2} + \frac{B}{(1-x) + (1+x)u^2}.
    \]
    En mettant au même dénominateur le terme de droite, puis, par identification des coefficients, on trouve $A(1-x) + B = 1$ et $A(1+x) + B = -1$. En résolvant ce système, on trouve $A = -1/x$ et $B = 1/x$. Et donc
    \[
      \frac{1-u^2}{((1-x) + (1+x)u^2)(1+u^2)} = -\frac{1}{x} \frac{1}{1+u^2} + \frac{1}{x}\frac{1}{(1-x) + (1+x)u^2}.
    \]
    Et comme pour $\alpha, \beta > 0$, on a $\int_0^{+\infty} \frac{\dd u}{\alpha + \beta u^2} = \left[\frac{1}{\sqrt{\alpha\beta}}\arctan\left(\frac{\sqrt{\beta}}{\sqrt{\alpha}}u\right)\right]_0^{+\infty} = \frac{\pi}{2\sqrt{\alpha\beta}}$, on obtient
    \begin{align*}
      \frac{1}{\pi}\int_0^\pi \frac{\cos(t)}{1-x\cos(t)}\dd t
      &= -\frac{2}{\pi x}\int_0^{+\infty} \frac{\dd u}{1+u^2} + \frac{2}{\pi x}\int_0^{+\infty} \frac{\dd u}{(1-x) + (1+x)u^2}\\
      &= -\frac{2}{\pi x}\frac{\pi}{1} + \frac{2}{\pi x}\frac{1}{2\sqrt{(1-x)(1+x)}}
      = -\frac{1}{x} + \frac{1}{x\sqrt{1-x^2}}.
    \end{align*}
    \item\label{2d} En combinant l'équation de la question \eqref{2b} et le résultat de la question \eqref{2c}, on obtient pour $x\in]0,1[$
    \[
      xF'(x) = -F(x) + \frac{1}{1+x} - \frac{1}{x} + \frac{1}{x\sqrt{1-x^2}},
    \]
    ce qui montre que $F$ est solution de l'équation différentielle de la question \eqref{2d}.

    De plus, en appliquant le résultat de la question \eqref{2b} avec $x=0$, on obtient $0 = -F(0) + 1 + 1/\pi\int_0^\pi \cos(t) \dd t = -F(0) + 1$, donc $F(0) = 1$.
    Cela établit que $F$ vérifie, en plus, la condition au bord $F(0) = 1$.

  \end{enumerate}
\end{solution}


% -----------------------------------------------
\begin{exo}\emph{(Intégrales impropres dépendant d'un paramètre)}

  Soit $f:[0,+\infty[\longrightarrow\mathbb R$ une fonction continue. La {\bf transformée de Laplace} de $f$ est la fonction $\mathcal{L}f$ définie par\vspace{-7pt}
  \[
    (\mathcal{L} f)(s)=\int_{0}^{+\infty}f(t)e^{-st}\dd t,
  \]
  pour les $s$ réels tels que l'intégrale converge.

  % -----------
  \textbf{Partie I.} Pour chaque fonction suivante, déterminer les valeurs réelles de $s$ pour lesquelles leur transformée de Laplace est définie et les calculer.
  \begin{enumerate}
      \item\label{q1} $e_\alpha : t\mapsto e^{\alpha t}$, $\alpha\in\mathbb{R}$.
      \item $g:t\mapsto t$.
  \end{enumerate}
  % -----------
  \textbf{Partie II.} Soit $f:[0,+\infty[\longrightarrow\mathbb R$ une fonction continue.
  On suppose qu'il existe $s_0\in\mathbb{R}$ tel que\vspace{-17pt}
  \[
    \sup_{t\geq 0} e^{-s_0t}{|f(t)|}<+\infty.
    \vspace{7pt}
  \]
  \begin{enumerate}[resume]
    \item Montrer que pour tout $a>s_0$, $\int_0^{+\infty} f(t)e^{-at}\dd t$ converge absolument.
    \item En déduire que $\mathcal{L}f$ est définie et continue sur $]s_0,+\infty[$.
    \item En déduire que $\lim_{s\to +\infty}(\mathcal{L}f)(s)=0$.
    \item Montrer que pour tout $a>s_0$, l'intégrale $\int_{0}^{+\infty}tf(t)e^{-at}\dd t$ converge absolument.
    \item En déduire que $\mathcal{L}f$ est de classe $C^1$ sur $]s_0,+\infty[$ et que, pour tout $s>s_0$, sa dérivée $(\mathcal{L}f)'$ vérifie\vspace{-14pt}
    \[
      (\mathcal{L}f)'(s)=-\int_{0}^{+\infty}tf(t)e^{-st}\dd t.
    \]
    \item On suppose de plus que $f$ est de classe $C^1$ sur $[0,+\infty[$. Montrer que $\mathcal{L}f'$, la transformée de Laplace de $f'$, est définie sur $]s_0,+\infty[$ et que
    \[
      (\mathcal{L}f')(s)=s(\mathcal{L}f)(s)-f(0), \text{ pour tout } s> s_0.
    \]
    \vspace{-20pt}
    \item Montrer que, pour tout $n\in\mathbb{N}^{*}$, la transformée de Laplace $\mathcal{L}g_n$ de $g_n(t) = t^n$, est bien définie sur $\mathbb{R}^*_{+}$ et la calculer.\\
    \begin{indication}
      Le calcul peut être fait par récurrence en utilisant la question précédente.
    \end{indication}
  \end{enumerate}
  % -----------
  \sisujet{%
    \begin{center}%
      \medskip\hrule\smallskip%
      \emph{La partie qui suit est en bonus.}%
      \smallskip\hrule\medskip%
    \end{center}%
  }
  \textbf{Partie III.} On considère l'équation différentielle sur $\mathbb{R}$ suivante
  \[
    (E) \qquad y''-3y'+2y=e^{3t},\quad y(0)=1 \text{ et } y'(0)=0.
  \]
  On suppose qu'il existe une solution $f\in C^{2}(\mathbb{R}_+)$ de cette équation pour laquelle il existe
  $s_0\geq 3$ tel que $\sup_{t\geq 0} e^{-s_0t}{|f(t)|}<+\infty$ et $\sup_{t\geq 0} e^{-s_0t}{|f'(t)|}<+\infty$.
  \begin{enumerate}[resume]
    \item Montrer que pour tout $s>s_0$, on a\vspace{-4pt}
    \[
      (s-1)(s-2)(\mathcal{L}f)(s)=\frac 1{s-3}+s-3.
    \]\vspace{-17pt}
    \item Décomposer en éléments simples la fraction rationnelle $\frac{s^2-6s+10}{(s-1)(s-2)(s-3)}$.
    \item En utilisant la question (\ref{q1}), en déduire une fonction $f$ telle que $(\mathcal{L}f)(s)=\frac{s^2-6s+10}{(s-1)(s-2)(s-3)}$ et vérifier qu'elle est une solution de $(E)$ sur $\mathbb{R}$.
  \end{enumerate}
\end{exo}
\begin{solution}
     \textbf{Partie I.}
  \begin{enumerate}
      \item
      Soient $s\in\mathbb{R}$ et $t_0>0$.\\
      Si $s=\alpha$ alors pour tout $t>0$, $e_\alpha(t)e^{-st}=1$ et l'intégrale $\displaystyle \int_0^{+\infty} e_\alpha(t)e^{-st}dt$ diverge donc $\mathcal{L}e_\alpha(\alpha)$ n'est pas définie.\\
      Lorsque $s\neq \alpha$, on a :
      \begin{align*}
          \int_0^{t_0} e_\alpha(t)e^{-st}dt
          &= \int_0^{t_0} e^{t(\alpha-s)}dt\\
          &=\frac{e^{t_0(\alpha-s)}}{\alpha-s}-\frac{1}{\alpha-s}.
      \end{align*}
      Si $s<\alpha$
      \begin{align*}
          \lim_{t_0\to +\infty} \int_0^{t_0} e_\alpha(t)e^{-st}dt
          &=+\infty
      \end{align*}
      et si $s>\alpha$
      \begin{align*}
          \lim_{t_0\to +\infty} \int_0^{t_0} e_\alpha(t)e^{-st}dt
          &=\frac{1}{s-\alpha}
      \end{align*}
      Ainsi $\mathcal{L}e_\alpha$ est définie sur $]\alpha,+\infty[$ et pour tout $s>\alpha$, on a
      \[
          (\mathcal{L}e_\alpha)(s)=\frac{1}{s-\alpha}.
      \]
      \item \label{q2} Soient $s\in\mathbb{R}$.

       Si $s=0$, alors pour tout $t>0$, $g(t)e^{-st}=t$ et l'intégrale $\displaystyle \int_0^{+\infty} g(t)e^{-st}dt$ diverge.

       Si $s\neq0$, pour tout $t_0>0$, on a
        \begin{align*}
          \int_0^{t_0} g(t)e^{-st}dt
          &= \int_0^{t_0} te^{-st}dt\\
          &=-\frac{t_0e^{-st_0}}{s}-\frac{e^{-st_0}}{s^2} +\frac1{s^2}
      \end{align*}
       Il s'en suit que pour $s<0$
      \begin{align*}
          \lim_{t_0\to +\infty} \int_0^{t_0} g(t)e^{-st}dt
          &=+\infty
      \end{align*}
      et pour $s>0$
      \begin{align*}
          \lim_{t_0\to +\infty} \int_0^{t_0} e_\alpha(t)e^{-st}dt
          &=\frac{1}{s^2}
      \end{align*}
       Ainsi $\mathcal{L}g$ est définie sur $]\alpha,+\infty[$ et pour tout $s>\alpha$, on a
      \[
          (\mathcal{L}g)(s)=\frac{1}{s^2}.
      \]
  \end{enumerate}
  % -----------
  \textbf{Partie II.}
  \begin{enumerate}[resume]
    \item On note $M= \sup_{t\geq 0} e^{-s_0t} |f(t)|$. Soit $a>s_0$. Pour tout $t\geq 0$, on a
    \[
        0\leq |f(t)|e^{-at}=|f(t)|e^{-s_0t}e^{-t(a-s_0)}\leq Me^{-(a-s_0)t}.
    \]
    Or, comme $a>s_0$, $0>-(a-s_0)$ donc $\int_0^{+\infty} e^{-(a-s_0)t}dt$ converge. Par comparaison, on en déduit que  $\int_0^{+\infty} |f(t)|e^{-at}$ converge et donc que $\int_0^{+\infty} f(t)e^{-at}$ converge absolument.
    \item Soit $\phi:[0,+\infty[\times\mathbb{R}\to\mathbb{R}$ définie par $\phi(t,s)= f(t)e^{-st}$ et soit $a>s_0$.

    Comme $f$ est continue sur $[0,+\infty[$, $\phi$ est continue sur $[0,+\infty[\times ]a,+\infty[$.

    De plus, pour tout $(t,s)\in [0,+\infty[\times ]a,+\infty[$, on a
    \[
        |\phi(t,s)|\leq |f(t)|e^{-at}
    \]
    et $\int_0^{+\infty} |f(t)|e^{-at}$ converge.

    Le théorème de continuité des intégrales impropres à paramètres implique alors que $\mathcal{L}f$ est définie et continue sur $]a,+\infty[$.

    Ainsi, étant donnée $s_1>s_0$, il existe $a\in\mathbb{R}$ tel que $s_0<a<s_1$. $\mathcal{L}f$ est donc bien définie et continue en $s_1$. Il s'en suit que $\mathcal{L}f$ est définie et continue sur $]s_0,+\infty[$.
    \item
    Soit $M= \sup_{t\geq 0} e^{-s_0t} |f(t)|$ et $s>s_0$. On a
    \[
        0\leq |f(t)|e^{-st}=|f(t)|e^{-s_0t}e^{-t(s-s_0)}\leq Me^{-(s-s_0)t}.
    \]
    Comme $\displaystyle \int_0^{+\infty} e^{-(s-s_0)t}dt=\frac1{s-s_0}$, on a
    \[
        0\leq \left|(\mathcal{L}f)(s)\right| \leq \frac M{s-s_0}
    \]
    et lorsque $s$ tend vers $+\infty$, on obtient $\lim_{s\to +\infty} (\mathcal{L}f)(s)=0$.
    \item
    On note toujours $M= \sup_{t\geq 0} e^{-s_0t} |f(t)|$.  On a pour tout $t\geq 0$
    \[
        0\leq t|f(t)|e^{-at}=|f(t)|e^{-s_0t}te^{-t(a-s_0)}\leq Mte^{-(a-s_0)t}.
    \]
    Or, comme $a>s_0$, $0>-(a-s_0)$ donc $\int_0^{+\infty} te^{-(a-s_0)t}dt$ converge.
    Par comparaison, on en déduit que  $\int_0^{+\infty} t|f(t)|e^{-at}$ converge et donc que $\int_0^{+\infty} tf(t)e^{-at}$ converge absolument.

    \item Soit encore $\phi:[0,+\infty[\times\mathbb{R}\to\mathbb{R}$ définie par $\phi(t,s)= f(t)e^{-st}$ et soit $a>s_0$.

    La fonction $\phi$ est dérivable par rapport à $s$ sur $[0,+\infty[\times ]a,+\infty[$ et pour tout
    $(t,s)\in [0,+\infty[\times ]a,+\infty[$, $\displaystyle \frac{\partial \phi}{\partial s}(t,s)= -tf(t)e^{-st}$ et comme $f$ est continue,  $\displaystyle \frac{\partial \phi}{\partial s}$ est continue sur $ [0,+\infty[\times ]a,+\infty[$.

    De plus, pour tout $(t,s)\in [0,+\infty[\times ]a,+\infty[$, on a
    \[
        \left| \displaystyle \frac{\partial \phi}{\partial s}(t,s) \right|\leq t|f(t)|e^{-at}
    \]
    et $\int_0^{+\infty} t|f(t)|e^{-at}$ converge.

    Le théorème de dérivation des intégrales impropres à paramètres implique alors que $\mathcal{L}f$ est de classe $C^1$ sur $]a,+\infty[$ et pour tout $s>a$, on a
    \[
        (\mathcal{L}f)'(s)=\int_0^{+\infty} -tf(t)e^{-st}dt
    \]
    Ainsi, étant donnée $s_1>s_0$, il existe $a\in\mathbb{R}$ tel que $s_0<a<s_1$. $\mathcal{L}f$ est dérivable en $s_1$, et $(\mathcal{L}f)'$ est continue en $s_1$. Il s'en suit que $\mathcal{L}f$ est de classe $C^1$ sur $]s_0,+\infty[$  et pour tout $s>s_0$,
    \[
      (\mathcal{L}f)'(s)=-\int_{0}^{+\infty}tf(t)e^{-st}\dd t.
    \]
    \item \label{q8} Soit $s>s_0$ et $t_0>0$. Une intégration par partie donne :
    \begin{align*}
        \int_0^{t_0} f'(t)e^{-st}dt
        &= \left[ f(t)e^{-st}\right]_0^{t_0} +s\int_0^{t_0} f(t) e^{-st}dt\\
        &= f(t_0)e^{-st_0} -f(0) +s\int_0^{t_0} f(t) e^{-st}dt.
    \end{align*}
    Notons encore $M= \sup_{t\geq 0} e^{-s_0t} |f(t)|$. Comme pour tout $s>s_0$ :
    \[
         0\leq |f(t_0)|e^{-st_0}=|f(t_0)|e^{-s_0t_0}e^{-t_0(s-s_0)}\leq Me^{-(s-s_0)t_0}.
    \]
    Puisque $s>s_0$, on en déduit que $\displaystyle \lim_{t_0\to +\infty} f(t_0)e^{-st_0}=0$
    D'autre part, comme $s>s_0$, on a $displaystyle \lim_{t_0\to +\infty}\int_0^{t_0} f(t) e^{-st}dt=(\mathcal{L}f)(s)$.

    Ainsi pour $s>s_0$ :
    \begin{align*}
        \lim_{t_0\to +\infty} \int_0^{t_0} f'(t)e^{-st}dt
        &= -f(0) +s(\mathcal{L}f)(s).
    \end{align*}
    Cela montre que pour tout $s>s_0$, $(\mathcal{L}f')(s)$ est bien définie et satisfait
    \[
      (\mathcal{L}f')(s)=s(\mathcal{L}f)(s)-f(0).
    \]
    \vspace{-20pt}
    \item On montre par récurrence que pour tout $n\in\mathbb{N}^*$,
    $\displaystyle(\mathcal{L}g_n)(s)=\frac{n!}{s^{n+1}}$.

    D'après la question (\ref{q2}), $\mathcal{L}g_1$ est définie sur $]0,+\infty[$ et pour tout $s>0$, $(\mathcal{L}g_1)(s)=\frac{1}{s^2}.$

    Supposons avoir montrer que $\mathcal{L}g_n$ est définie sur $]0,+\infty[$ et que, pour tout $s>0$, $(\mathcal{L}g_n)(s)=\frac{n!}{s^{n+1}}.$

    La fonction fonction $g_{n+1}$ est de classe $C^1$ sur $[0,+\infty[$ et comme $\lim_{t\to +\infty} t^{n+1}e^{-s_0t}=0$ quel que soit $s_0>0$, $\sup_{t>s_0}|g_{n+1}(t)|e^{-s_0t}$ est fini.

    D'après la question précédente, pour tout $s_0>0$ et tout $s>s_0$, on a :
    \begin{align*}
        (\mathcal{L}g_{n+1})(s)
        &=\frac1s (\mathcal{L}g'_{n+1})(s)\\
        &=\frac1s (\mathcal{L}(n+1)g_n)(s)\\
        &=\frac{(n+1)}s\frac{ n!}{s^{n+1}}\\
         &=\frac{ (n+1)!}{s^{n+2}}.
    \end{align*}
    On en déduit que pour tout $s>0$, $(\mathcal{L}g_{n+1})(s)=\frac{ (n+1)!}{s^{n+2}}.$

    Ainsi, nous avons montré par récurrence que pour tout $n\in\mathbb{N}^*$, $\mathcal{L}g_n$ est définie sur $]0,+\infty[$ et que, pour tout $s>0$, $(\mathcal{L}g_n)(s)=\frac{n!}{s^{n+1}}.$
  \end{enumerate}
  % -----------
  \sisujet{%
    \begin{center}%
      \medskip\hrule\smallskip%
      \emph{La partie qui suit est en bonus.}%
      \smallskip\hrule\medskip%
    \end{center}%
  }
  \textbf{Partie III.} On considère l'équation différentielle sur $\mathbb{R}$ suivante
  \[
    (E) \qquad y''-3y'+2y=e^{3t},\quad y(0)=1 \text{ et } y'(0)=0.
  \]
  On suppose qu'il existe une solution $f\in C^{2}(\mathbb{R}_+)$ de cette équation pour laquelle il existe
  $s_0\geq 3$ tel que $\sup_{t\geq 0} e^{-s_0t}{|f(t)|}<+\infty$ et $\sup_{t\geq 0} e^{-s_0t}{|f'(t)|}<+\infty$.
  \begin{enumerate}[resume]
    \item
    La fonction fonction $f$ satisfait $f''-f'+2f=e_3$ où $e_3$ est la fonction introduite dans la question \ref{q1}. On en déduit que
    \begin{align*}
        \mathcal{L}e_3
        &=\mathcal{L}({f''-3f'+2f})\\
        &=\mathcal{L}f'' - \mathcal{L}f'+2\mathcal{L}f.
    \end{align*}
    D'une part, d'après la question (\ref{q8}) donne pour tout $s>s_0$
    \begin{align*}
        (\mathcal{L}f'')(s) - 3(\mathcal{L}f')(s)+2(\mathcal{L}f)(s)
        &=s(\mathcal{L}f')(s)-f'(0)-3\left(s\mathcal{L}f(s)-f(0)\right)-(\mathcal{L}f)(s)\\
        &=s\left(\left(s\mathcal{L}f(s)-f(0)\right)-f'(0)\right) -3\left(s\mathcal{L}f-f(0)\right)+2(\mathcal{L}f)(s).
    \end{align*}
    En tenant compte du fait que $f'(0)=0$ et $f(0)=1$, il vient :
    \begin{align*}
        (\mathcal{L}f'')(s) -3 (\mathcal{L}f')(s)+2(\mathcal{L}f)(s)
            &=(s^2-3s+2)\mathcal{L}f(s)+3-s\\
            &=(s-2)(s-1)\mathcal{L}f(s)+3-s.
    \end{align*}
    D'autre part, d'après la question (\ref{q1}), pour tout $s>3$, on a $(\mathcal{L}e_3)(s)=\frac{1}{s-3}$.

    Ainsi, pour tout $s>s_0$, on a
    \[
      (s-1)(s-2)(\mathcal{L}f)(s)=\frac 1{s-3}+s-3.
    \]
    \item Il existe $\alpha,$ $\beta$ et $\gamma$ réels tels que pour tout $s$ :
    \[
      \frac{s^2-6s+10}{(s-1)(s-2)(s-3)}= \frac\alpha{s-1}+\frac\beta{s-2}+\frac\gamma{s-3}.
    \]
    En multpliant par $s-1$ et en évaluant en $s=1$, on obtient
    \[
        \alpha=\frac{1-6+10}{(-1)(-2)}=\frac52.
    \]
    En multpliant par $s-2$ et en évaluant en $s=2$, on obtient
    \[
        \beta=\frac{4-12+10}{(1)(-1)}=-2.
    \]
    En multpliant par $s-3$ et en évaluant en $s=3$, on obtient
    \[
        \gamma=\frac{9-18+10}{2}=\frac12.
    \]
    Ainsi
     \[
      \frac{s^2-6s+10}{(s-1)(s-2)(s-3)}= \frac52\frac1{s-1}-2\frac1{s-2}+\frac12\frac1{s-3}.
    \]

    \item Soit $f= \frac52e_1-2e_2+\frac12e_3$. Alors on a d'après la question (\ref{q1}) $(\mathcal{L}f)(s)=\frac{s^2-6s+10}{(s-1)(s-2)(s-3)}$ pour tout $s>3$.

    Vérifions si $f$ est une solution sur $\mathbb{R}$ de l'équation $(E)$. On a $f(0)=\frac52-2+\frac12=1$ et $f'(0)=\frac52-4+\frac32=0$. D'autre part pour tout $s\in\mathbb{R}$ :
    \begin{align*}
      f''(s)-3f'(s)+2f(s)
      &= \frac52e^s-8e^{2s}+\frac92e^{3s}-\frac{15}2e^s+12e^{2s}-\frac92e^{3s} +5e^s-4e^{2s}+e^{3s}\\
      &=e^{3s}.
    \end{align*}
    Ainsi $f$ est une solution de $(E)$ sur $\mathbb{R}$.
  \end{enumerate}
\end{solution}

\end{document}
