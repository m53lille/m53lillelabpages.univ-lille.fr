\documentclass{cours}
\usepackage[french]{babel}



\begin{document}
\univ\annee
\niveau{L3 Mathématiques - M53}
\auteur{W. Alexandre }
\donnees{Chapitre 1}{Rappels : intégrales généralisées et continuité uniforme}
\entete


\section{Continuité uniforme}
Dans cette section, pour $n\in\nn^*$, on se donne une norme $\|\cdot\|$ sur $\rr^n$. 
\begin{definition}
 Soit $E$ une partie de $\rr^n$ et $f:E\to\rr\text{ ou } \cc$. On dit que $f$ est \emph{uniformément continue} sur $E$ si pour tout $\varepsilon>0$, il existe $\eta>0$ tel que quels que soient $x,x'\in E$, $\|x-x'\|<\eta$ implique $|f(x)-f(x')|<\varepsilon$.
\end{definition}
\begin{proposition}
La continuité uniforme implique la continuité.
\end{proposition}
\begin{remarque}
 La réciproque est fausse. Par exemple $f:x\mapsto x^2$ est continue sur $\rr$ mais n'est pas uniformément continue sur $\rr$ (voir TD).
\end{remarque}
\begin{theoreme}[Heine]
Toute fonction continue sur un compact y est uniformément continue.
\end{theoreme}
\pr Soit $E$ une partie compacte de $\rr^n$ et, soit $f:E\to\rr\text{ ou  }\cc$ continue sur $E$. 

Supposons que $f$ ne soit pas uniformément continue sur $E$, autrement qu'il existe  $\varepsilon_0>0$ tel que pour tout $\eta>0$, il existe $x$ et $y$ dans $E$ tels que $\|x-y\|<\eta$ et $|f(x)-f(y)|\geq \varepsilon_0$.

On en déduit qu'il existe deux  suites $(x_n)_n$ et $(y_n)_n$ d'éléments de $E$ telles que pour tout $n$, $\|x_n-y_n\|<\frac1n$ et $|f(x_n)-f(y_n)|\geq \varepsilon_0$.

Comme $E$ est fermé et borné, d'après le théorème de Bolzano-Weierstrass, on peut extraire de $(x_n)_n$ une suite $(x_{n_k})_k$ convergente. On note $\alpha\in E$ la limite de $(x_{n_k})_k$. Puisque
\begin{align*}
 \|y_{n_k}-\alpha\|
 &\leq \|y_{n_k}-x_{n_k}\|+\|x_{n_k}-\alpha\|\\
 &\leq \frac1{n_k}+\|x_{n_k}-\alpha\|,
\end{align*}
la suite $(y_{n_k})_k$ converge également vers $\alpha$. 

La continuité de $f$ implique :
\begin{align*}
 \lim_{k\to +\infty} f(x_{n_k})-f(y_{n_k})
 &= \lim_{k\to +\infty} f(x_{n_k})-f(\alpha) + f(\alpha)-f(y_{n_k})\\
 &=0,
\end{align*}
ce qui est contradictoire avec le fait que $|f(x_n)-f(y_n)|\geq \varepsilon_0$.\qed

\section{Intégrales généralisées (impropres)}
\subsection{Intégrale de Riemann}
Soient $a$ et $b$ deux réels, $a<b$.

On dit que $\phi:[a,b]\to\rr$ est une {\em fonction en escalier} s'il existe une subdivision $a_0=a<a_1<\ldots<a_n=b$ et des réels $c_1,\ldots, c_n$ tels que pour tout $i\in\{1,\ldots, n\}$ et tout $x\in ]a_{i-1},a_i[$, on ait $\phi(x)=c_i$.  
On définit alors
$$\int_a^b \phi(x)dx=\sum_{i=1}^nc_i(a_i-a_{i-1}).$$

Soit $f:[a,b]\to\rr$ une fonction bornée. On pose
\begin{align*}
 I^-(f)=\sup\left\{
	\int_a^b\phi(x)dx\ /\ \phi \text{ fonction en escalier et } \phi(x)\leq f(x),\ \forall x\in[a,b]
	\right\},\\
I^+(f)=\inf\left\{
	\int_a^b\phi(x)dx\ /\ \phi \text{ fonction en escalier et } f(x)\leq \phi(x),\ \forall x\in[a,b]
	\right\}.
\end{align*}

On dira alors que $f$ est {\em Riemann-intégrable} ou {\em intégrable au sens de Riemann} si $I^+(f)=I^-(f)$. On appelle ce nombre {\em intégrale de Riemann de $f$} et on le note
$$\int_a^bf(x)dx= I^+(f)=I^-(f).$$
\begin{exemple}
 Les fonctions en escaliers, les fonctions continues, continues par morceaux ($f:[a,b]\to\rr$ est continue par morceaux s'il existe une subdivision $a_0=a<a_1<\ldots<a_n$ telle que pour tout $i$, $f|_{]a_i,a_{i+1}[}$ est continue et les limites $\lim_{\genfrac{}{}{0pt}{}{x\to a_i}{a_i<x}}f(x)$ et $\lim_{\genfrac{}{}{0pt}{}{x\to a_{i+1}}{a_{i+1}>x}}f(x)$ existent), les fonctions monotones sont Riemann-intégrables.
\end{exemple}

\subsection{Intégrabilité locale d'une fonction}
On se donne $I$ un intervalle quelconque de $\rr$ (donc non nécessairement borné ou  fermé...)
\begin{definition}
Une fonction $f:I\to\rr$ est dite {\em localement intégrable } sur $I$ si sa restriction à tout intervalle fermé et borné $[c,d]$ inclus dans $I$ est Riemann-intégrable. 
\end{definition}
\begin{exemple}
 Toute fonction continue sur $I$ est localement intégrable sur $I$.\\
 Toute fonction continue par morceaux sur $I$ est localement intégrable sur $I$.\\
 Toute fonction monotone sur $I$ est localement intégrable sur $I$.
 \end{exemple}

\subsection{Intégrales généralisées, intégrales impropres}
\begin{definition}
 Soit $a\in\rr$, $b\in\rr\cup \{+\infty\}$ (resp. $a\in\rr\cup\{-\infty\}$, $b\in\rr$) et $f$ une fonction localement intégrable sur $[a,b[$ .
 
 On dit que {\em l'intégrale  de $f$ sur $[a,b[$ est convergente} si $\lim_{\genfrac{}{}{0pt}{}{x\to b}{x<b}}\int_a^xf(t)dt$ (resp. 
 $\lim_{\genfrac{}{}{0pt}{}{x\to a}{x>a}}\int_x^bf(t)dt$)
 existe et appartient à $\rr$. On appelle cette limite {\em l'intégrale généralisée (ou impropre) } de $f$ sur $[a,b[$  et on la note $\int_a^bf(t)dt$.
 Si la limite n'existe pas, on dit que l'intégrale $\int_a^bf(t)dt$ est divergente en $b$ (resp. en $a$).
 
\par\bigskip 
 
 Soit  $a\in\rr\cup\{-\infty\}$, $b\in\rr\cup \{+\infty\}$ et  $f$ une fonction localement intégrable sur $]a,b[$.

 On dit que l'intégrale de $f$ sur $]a,b[$ est convergente s'il existe $c\in]a,b[$ tel que $\int_a^cf(t)dt$ et $\int_c^bf(t)dt$ convergent. Dans ce cas, on pose $\int_a^bf(t)dt=\int_a^cf(t)dt+\int_c^bf(t)dt$. Sinon on dit que l'intégrale $\int_a^bf(t)dt$ est divergente.
 
 \end{definition}
 \begin{remarque}
  Il faut noter que si $f$ est localement intégrable sur $[a,b[$, la nature de l'intégrale généralisée $\int_a^bf(t)dt$ ne dépend pas de $a$, autrement dit pour tout $a'\in[a,b[$, $\int_a^bf(t)dt$ et $\int_{a'}^bf(t)dt$ sont de même nature. En effet, pour tout $a<a'<x$ on a $\int_a^xf(t)dt=\int_{a'}^af(t)dt +\int_{a'}^xf(t)dt$.
  
  Ainsi  
   $\lim_{\genfrac{}{}{0pt}{}{x\to b}{x<b}} \int_a^xf(t)dt=\int_{a'}^af(t)dt +\lim_{\genfrac{}{}{0pt}{}{x\to b}{x<b}} \int_{a'}^xf(t)dt$ et donc $\int_a^bf(t)dt$ et $\int_{a'}^bf(t)dt$ sont de même nature.
   
    D'autre part, dans la deuxième partie de la définition ci-dessus $\int_a^bf(t)dt$ ne dépend pas du point $c$. En effet, si $c'$ est un point de l'intervalle $]a,b[$, distinct de $c$, par exemple, $c'<c$, alors pour tout $x\in]a,c'[$, la relation de Chasles donne
 \begin{align*}
  \int_{x}^{c'}f(t)dt&=\int_x^cf(t)dt -\int_{c'}^cf(t)dt
 \end{align*}
d'où
\begin{align*}
 \lim_{\genfrac{}{}{0pt}{}{x\to a}{x>a}} \int_{x}^{c'}f(t)dt =  \int_{a}^{c}f(t)dt -\int_{c'}^cf(t)dt.
\end{align*}
Autrement dit, l'intégrale généralisée $\int_{a}^{c'}f(t)dt$ converge vers $\int_{a}^{c}f(t)dt -\int_{c'}^cf(t)dt.$
De même, $\int_{c'}^bf(t)dt$ converge vers $\int_{c}^{b}f(t)dt +\int_{c'}^cf(t)dt$ et
\begin{align*}
 \int_{a}^{c'}f(t)dt + \int_{c'}^bf(t)dt &=\int_{a}^{c}f(t)dt + \int_{c}^bf(t)dt.
\end{align*}

L'étude d'une intégrale impropre en les deux bornes se ramenant à l'étude de deux intégrales impropres en une seule borne, nous étudierons essentiellement uniquement le cas d'intégrales impropres en exactement une borne.

 \end{remarque}

 \begin{exemple}
  \'Etude de $\int_a^{+\infty} \frac1{t^\alpha} dt$ pour $a>0$ et $\alpha>0$ : Soit $x>a$. Lorsque $\alpha\ne 1$, on a:
  \begin{align*}
   \int_a^x\frac1{t^\alpha} dt&= \frac1{1-\alpha}\frac1{x^{\alpha-1}}-\frac1{1-\alpha}\frac1{a^{\alpha-1}}.
  \end{align*}
Si $\alpha >1$, $\lim_{\genfrac{}{}{0pt}{}{x\to +\infty}{x>a}} \frac1{1-\alpha}\frac1{x^{\alpha-1}}=0$ et donc l'intégrale $\int_a^{+\infty} \frac1{t^\alpha} dt$ converge et vaut $-\frac1{1-\alpha}\frac1{a^{\alpha-1}}.$

Si $\alpha<1$, $\lim_{\genfrac{}{}{0pt}{}{x\to +\infty}{x>a}} \frac1{1-\alpha}\frac1{x^{\alpha-1}}=+\infty$ et donc l'intégrale 
 $\int_a^{+\infty} \frac1{t^\alpha} dt$ diverge.
  
Enfin, si $\alpha=1$,
\begin{align*}
   \int_a^x\frac1{t} dt&= \ln(x)-\ln(a)\xrightarrow[x\to +\infty]{}+\infty
  \end{align*}
  et donc $\int_a^{+\infty}\frac1tdt$ diverge.
 \end{exemple}
 
 \begin{exemple}
  \'Etude de $\int_0^b \frac1{t^\alpha} dt$ pour $b>0$ et $\alpha>0$ : Soit $0<x<b$. On a si $\alpha\ne 1$ :
  \begin{align*}
   \int_x^b\frac1{t^\alpha} dt&= \frac1{1-\alpha}\frac1{b^{\alpha-1}}-\frac1{1-\alpha}\frac1{x^{\alpha-1}}.
  \end{align*}
Si $\alpha >1$, $\lim_{\genfrac{}{}{0pt}{}{x\to 0}{x>0}} -\frac1{1-\alpha}\frac1{x^{\alpha-1}}=+\infty$ et donc l'intégrale $\int_0^b \frac1{t^\alpha} dt$ diverge. 

Si $\alpha<1$, $\lim_{\genfrac{}{}{0pt}{}{x\to 0}{x>0}} -\frac1{1-\alpha}\frac1{x^{\alpha-1}}=0$ et donc l'intégrale 
 $\int_0^b \frac1{t^\alpha} dt$ converge et vaut $\frac1{1-\alpha}\frac1{b^{\alpha-1}}.$
  
Enfin, si $\alpha=1$,
\begin{align*}
   \int_x^b\frac1{t} dt&= \ln(b)-\ln(x)\xrightarrow[x\to 0^+]{}+\infty
  \end{align*}
  et donc $\int_0^b\frac1tdt$ diverge.
 \end{exemple}

 \begin{exemple}
Montrons que $\int_{-\infty}^{+\infty}\frac1{1+t^2}dt$ converge : Soit $x>0$
\begin{align*}
 \int_0^x \frac1{1+t^2}dt&=\arctan x \xrightarrow[x\to +\infty]{} \frac\pi2.
 \end{align*}
 Soit $x<0$
 \begin{align*}
 \int_{x}^0 \frac1{1+t^2}dt&=-\arctan x \xrightarrow[x\to -\infty]{} \frac\pi2.
\end{align*}
Ainsi $\int_{-\infty}^{+\infty}\frac1{1+t^2}dt$ converge et vaut $\pi$.
\end{exemple}
 
 \begin{remarque}
 Contrairement à ce que ces exemples semblent indiquer, le fait que $\int_1^{+\infty} f(t)dt$ converge n'implique pas que $\lim_{x\to +\infty} f(x)=0$, même si $f$ est positive. Considérons par exemple la fonction $f$ définie sur $[0,+\infty[$ de la manière suivante :
 $f(x)=1$ s'il existe $n\in\nn$ tel que $x$ appartienne à $[n,n+\frac1{2^n}]$, $f(x)=0$ sinon. 
 Soit $x>0$ et $n_x\in\nn$ tel que $n_x-1<x\leq n_x$. Alors
 \begin{align*}
\int_0^xf(t)dt&\leq \int_0^{n_x}f(t)dt\\
&=\sum_{k=0}^{n_x-1} \frac1{2^k}\\
&=\frac{1-\frac{1}{2^{{n_x}}}}{1-\frac12}
 \end{align*}
et
 \begin{align*}
\int_0^xf(t)dt&\geq \int_0^{{n_x}-1}f(t)dt\\
&=\sum_{k=0}^{{n_x}-2} \frac1{2^k}\\
&=\frac{1-\frac{1}{2^{{n_x}-1}}}{1-\frac12}
 \end{align*}

Lorsque $x$ tend vers $+\infty$, ${n_x}$ tend vers $+\infty$ et $\frac{1-\frac{1}{2^{{n_x}}}}{1-\frac12}$ et $\frac{1-\frac{1}{2^{{n_x}-1}}}{1-\frac12}$ tendent vers $2$. Par conséquent,  $\lim_{x\to +\infty} \int_0^xf(t)dt=2$ et donc l'intégrale généralisée $\int_0^{+\infty} f(t)dt$ converge alors que $f$ ne tend pas vers 0.
\end{remarque}

\subsection{Critères de convergence}

\begin{proposition}
 Soient $a,b\in\rr$ finis, $a<b$, et $f$ localement Riemann intégrable sur $[a,b[$  et bornée sur $[a,b[$. Alors l'intégrale généralisée $\int_a^b f(t)dt$ converge.
 \end{proposition}
 \begin{corollaire}
  Si $f$ est continue sur $[a,b[$, $b$ fini, et si $\lim_{\genfrac{}{}{0pt}{}{x\to b}{x<b}} f(x)$ existe, alors l'intégrale généralisée $\int_a^b f(t)dt$ converge.
 \end{corollaire}





\begin{notation}
Soient $a\in\rr\cup \{\pm\infty\}$ et deux fonctions $f$ et $g$ définies au voisingae de $a$. On dit que $f$ est dominée par $g$ au voisinage de $a$ et on note $f(x)\underrel{=}{x\to a} \co\left(g(x)\right)$ s'il existe $M\geq0$ telle que pour tout $x$ au voisinage de $a$, $|f(x)|\leq M|g(x)|$.
\end{notation}


 \begin{theoreme}
 Soient $a\in\rr$ et $b\in\rr\cup\{+\infty\}$ et $f$ et $g$ deux fonctions localement intégrables sur $[a,b[$. On suppose que 
 \begin{enumerate}[(i)]
  \item Il existe $c\in[a,b[$ tel que pour tout $x\in[c,b[$, $f(x)\geq 0$ et $g(x)\geq 0$
  \item $f(x)\underrel={x\underrel{\rightarrow}{x<b}b}\co(g(x))$.
 \end{enumerate}
On a 
  \begin{enumerate}[(a)]
  \item si $\int_a^bg(t)dt$ converge alors $\int_a^b f(t)dt$ converge.
  \item si $\int_a^bf(t)dt$ diverge alors $\int_a^b g(t)dt$ diverge.
 \end{enumerate}
\end{theoreme}
\begin{exemple}
\'Etudions la convergence de l'intégrale $\int_2^{+\infty}\frac1{t^{2-1/t}}dt$.
Remarquons tout d'abord que ce n'est pas une intégrale du type $\int^{+\infty} \frac1{t^\alpha}dt$ car l'exposant n'est pas constant. 
On a $\lim_{x\to +\infty} \frac{\frac1{t^{2-1/t}}}{\frac1{t^{\frac32}}}=0$ donc $f(x)\underrel={x\to +\infty} \co(g(x))$.

Comme $\int_2^{+\infty}\frac1{t^{\frac32}}dt$ converge et comme $\frac1{t^{1-\frac1t}}\geq 0$ quel que soit $t\geq 2$, on en déduit que $\int_2^{+\infty}\frac1{t^{2-1/t}}dt$ converge.
\end{exemple}

\begin{definition}
 Soient $f$ et $g$ deux fonctions définies sur un intervalle non vide $I$, d'extrémité $a$ et $b$, $a<b$, et soit $x_0\in I$ ou $x_0=a$ ou $x_0=b$.
 
 On dit que $f$ est équivalente à $g$ en $x_0$ s'il existe une fonction $\varepsilon:I\to\rr$ telle que 
 \begin{enumerate}[(i)]
  \item $f=g\cdot (1+\varepsilon)$,
  \item $\lim_{\genfrac{}{}{0pt}{}{x\to x_0}{x\in I}} \varepsilon(x)=0$.
 \end{enumerate}
Dans ce cas, on note $f\sim_{x\to x_0} g$.

Lorsque que $g$ est non nulle sur un voisinage de $x_0$ sauf peut-être en $x_0$, cela est équivalent à dire que $\lim_{\genfrac{}{}{0pt}{}{x\to x_0}{x\in I}} \frac{f(x)}{g(x)}=1.$
\end{definition}

\begin{theoreme}
Soient $a\in\rr$ et $b\in\rr\cup\{+\infty\}$ et $f$ et $g$ deux fonctions localement intégrables sur $[a,b[$. On suppose que 
 \begin{enumerate}[(i)]
  \item Il existe $c\in[a,b[$ tel que pour tout $x\in[c,b[$, $f(x)\geq 0$,
  \item $f(x)\underrel\sim{x\underrel{\rightarrow}{x<b}b}g(x)$.
 \end{enumerate}
 Alors $\int_a^bg(t)dt$ converge si et seulement si $\int_a^b f(t)dt$ converge.
\end{theoreme}



\begin{exemple}
Soit $f:[1,+\infty[\to \rr$ définie par $f(t)=\frac1{t^{1+1/t}}$. La fonction $f$ n'est pas une fonction puissance puisque l'exposant n'est pas constant. Cependant, pour tout $t\geq 1$, $\frac1t>0$ et $f(t)\sim_{t\to+\infty} \frac1t$ car 
\begin{align*}
\lim_{t\to +\infty} \frac{f(t)}{\frac1t}&=\lim_{t\to +\infty} \frac1{t^{\frac1t}}\\
&= \lim_{t\to +\infty} e^{-\frac{\ln t}t}\\
&=1.
\end{align*}
Comme $\int_1^{+\infty} \frac 1tdt$ diverge, on en déduit que l'intégrale généralisée $\int_1^{+\infty}\frac1{t^{1+1/t}}dt$ diverge également.
\end{exemple}


\begin{definition}
 Soient $a\in\rr$ et $b\in\rr\cup\{+\infty\}$ tels que $a<b$ et $f$ une fonction localement intégrable sur $[a,b[$. 
On dit que l'intégrale généralisée $\int_a^bf(t)dt$ {\em converge absolument} si l'intégrale généralisée $\int_a^b |f(t)|dt$ converge.

Si l'intégrale généralisée $\int_a^bf(t)dt$ converge mais ne converge pas absolument, on dit que l'intégrale généralisée $\int_a^bf(t)dt$ est {\em semi-convergente.}
\end{definition}

\begin{theoreme}
Soient $a$ et $b$ deux nombres réels tels que $a<b$, éventuellement $a=-\infty$ ou $b=+\infty$, et $f$ une fonction localement intégrable sur $[a,b[$ . 
 Si l'intégrale généralisée $\int_a^bf(t)dt$ converge absolument alors elle converge.
\end{theoreme}

\begin{exemple}
\'Etudions $\int_0^{+\infty} \frac{\sin t}{1+t^2}$. Pour tout $t\in\rr$, nous avons $|\sin t|\leq 1$ et donc $\left|\frac{\sin t}{1+t^2}\right|\leq \frac{1}{1+t^2}$. Puisque l'intégrale généralisée $\int_0^{+\infty} \frac1{1+t^2}dt$ converge, le critère de comparaison des intégrales implique que l'intégrale généralisée $\int_0^{+\infty} \frac{\sin t}{1+t^2}dt$ converge absolument et donc converge.
\end{exemple}
\begin{exemple} 
L'intégrale $\int_1^{+\infty} \frac{\cos t}{t^2} dt$ est convergente. En effet, $t\mapsto \frac{\cos t}{t^2}$ est continue sur $[1,+\infty[$ donc localement intégrable et pour totu $t\geq 1$, on a $0\leq \left|\frac{\cos t}{t^2}\right|\leq \frac1{t^2}$. Comme $\int_1^{+\infty} \frac1{t^2}dt$ converge, on en déduit que $\int_1^{+\infty} \frac{\cos t}{t^2} dt$ converge absoluement et donc converge.
\end{exemple}
\begin{exemple}
 $\int_0^{+\infty} \frac{\sin t} tdt$ est semi-convergente.
 
 En effet, $t\mapsto \frac{\sin t}t$ est continue sur $]0,+\infty[$ et donc localement intégrable sur $]0,+\infty[$. De plus, $\lim_{\genfrac{}{}{0pt}{}{}{x\to 0}{x>0}} \frac{\sin t} t=1$ donc $\int_0^1\frac{\sin t}tdt$ converge.
 
 D'autre part pour tout $X\geq 1$, en effectuant une intégration par parties, on a 
 \begin{align*}
  \int_1^X\frac{\sin t}tdt&= \left[-\frac{\cos t}t\right]_1^X - \int_1^X\frac{\cos t}{t^2}dt\\
  &= \cos1 +\frac{\cos X}X - \int_1^X\frac{\cos t}{t^2}dt.
 \end{align*}
Comme $\int_1^{+\infty}\frac{\cos t}{t^2}dt$ converge, $\lim_{x\to +\infty} \int_1^X\frac{\cos t}{t^2}dt=\int_1^{+\infty}\frac{\cos t}{t^2}dt.$

D'autre part, puisque pour tout $X\geq 1$, $\left|\frac{\cos X}X\right|\leq \frac1X$, on a $\lim_{X\to +\infty} \frac{\cos X}X=0$.

Ainsi $\lim_{X\to +\infty}  \int_1^X\frac{\sin t}tdt=\cos1-\int_1^{+\infty}\frac{\cos t}{t^2}dt$, donc $\int_1^{+\infty} \frac{\sin t} tdt$  converge. 

Cependant $\int_1^{+\infty} \frac{\sin t} tdt$ n'est pas absolument convergente : pour tout $n\in\nn$, on a
\begin{align*}
 \int_0^{n\pi} \left|\frac{\sin t}t\right|dt
 &\geq\sum_{k=0}^{n-1} \int_{k\pi+\frac\pi4}^{(k+1)\pi-\frac\pi4} \frac{\sin t} tdt\\
 &\geq \sum_{k=0}^{n-1} \int_{k\pi+\frac\pi4}^{(k+1)\pi-\frac\pi4} \frac{\sqrt 2}2\frac1 tdt\\
 &\geq \frac{\sqrt 2}2 \sum_{k=0}^{n-1} \frac\pi2 \frac1{(k+1)\pi}\\
 &\geq \frac{\sqrt 2}{4} \sum_{k=0}^{n-1} \frac1{(k+1)}.
 \end{align*}
On en déduit que $\lim_{n\to +\infty} \int_0^{n\pi} \left|\frac{\sin t}t\right|dt=+\infty$ et donc que  $\int_0^{+\infty} \frac{\sin t} tdt$  ne converge pas absoluement.
\end{exemple}


\begin{definition}[Critère de Cauchy]
Soient $a$ et $b$ deux nombres réels tels que $a<b$, éventuellement $b=+\infty$, et $f$ une fonction localement intégrable sur $[a,b[$ .
On dit que $\int_a^bf(t)dt$ satisfait le critère de Cauchy pour les intégrales généralisées 
 si pour tout $\varepsilon>0$, il existe $x_\varepsilon\in[a,b[$  tel que pour tout $x,x'\in ]x_\varepsilon,b[$, on ait $$\left|\int_x^{x'} f(t)dt\right|<\varepsilon.$$
\end{definition}
\begin{theoreme}
 Soient $a\in\rr$ et $b\in\rr\cup\{+\infty\}$ tels que $a<b$ et $f$ une fonction localement intégrable sur $[a,b[$ . Alors l'intégrale généralisée $\int_a^bf(t)dt$ converge si et seulement elle satisfait le critère de Cauchy pour les intégrales généralisées.
\end{theoreme}
\end{document}
