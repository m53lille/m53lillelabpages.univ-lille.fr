\documentclass{cours}
\usepackage[french]{babel}
\begin{document}
\univ\annee
\niveau{L2 Mathématiques - M33}
\auteur{W. Alexandre }
\donnees{Chapitre 2}{INTÉGRALES DÉPENDANT D'UN PARAMÈTRE :\\
Cas des intégrales définies}
\entete


Dans ce chapitre, $a$ et $b$ désigneront deux réels tels que $a<b$ et $X$ désignera une partie de de $\rr^d$.

On considère la fonction
$$\app f {[a,b]\times X} \rr {(t,x)}{f(t,x)}$$
et on suppose (à minima) que quel que soit $x\in X$, l'application $t\mapsto f(t,x)$ est Riemann intégrable. On peut ainsi définir une nouvelle fonction $F$ en posant pour tout $x\in X$ :
$$F(x)=\int_a^b f(t,x)dt.$$
Dans ce chapitre nous allons donner des conditions suffisante sur $f$ pour que $F$ soit continue, dérivable, de classe $C^1$...

\section{Continuité}
\begin{lemme}\label{lemme1}
 Soient $X$ une partie de $\rr^d$, $\|\cdot\|$ une norme sur $\rr^d$, $a,b\in\rr$ vérifiant $a<b$, $g\in C([a,b]\times X)$. Alors pour tout $\varepsilon>0$, tout $x \in X$, il existe $\delta>0$ tel que pour tout $t\in[a,b]$, tout $x'\in X$, si $\|x-x'\|<\delta$ alors $|g(x',t)-g(x,t)|<\varepsilon$. 
 \end{lemme}
\pr On raisonne par l'absurde et on suppose qu'il existe $x_*\in X$ et $\varepsilon>0$ tel que pour tout $\delta>0$, il existe $t\in[a,b]$ et $x'\in X$ vérifiant $\|x'-x_*\|<\delta$ et $|g(t,x')-g(t,x_*)|\geq \varepsilon$.

Alors pour tout $n\in\nn^*$, il existe $t_n\in[a,b]$, $x_n\in X$ tel que $\|x_n-x_*\|<\frac 1n$ et $|g(t_n,x_n)-g(t_n,x_*)|\geq\varepsilon$.

En particulier, la suite $(x_n)_n$, comme toute sous-suite extraite de $(x_n)_n$, converge vers $x_*$.

D'autre part, $(t_n)_n$ est une suite d'éléments de $[a,b]$, donc d'après le théorème de Bolza\-no-Weierstrass, il existe une sous-suite extraite $(t_{n_k})_k$ qui converge dans $[a,b]$. On note $t_*$ la limite de $(t_{n_k})_k$.

Puisque $g$ est continue, $\lim_{k\to +\infty} g(t_{n_k},x_{n_k})=g(t_*,x_*)$ et $\lim_{k\to +\infty} g(t_{n_k},x_*)=g(t_*,x_*)$ et donc nous avons :
$$0<\varepsilon\leq \lim_{k\to +\infty} |g(t_{n_k},x_{n_k})- g(t_{n_k},x_*)| = 0,$$
ce qui est absurde !\qed

\begin{theoreme}[de continuité des intégrales à paramètres définies]
Soient $X$ une partie de $\rr^d$, $a,b\in\rr$, $a<b$, $f$ une fonction continue sur $[a,b]\times X$.
Alors la fonction $F:X\to \rr$ définie par $F(x)=\int_a^bf(t,x)dt$ est continue sur $X$.
\end{theoreme}
\pr On se donne une norme $\|\cdot\|$ sur $\rr^d$. On montre que pour tout $x_0\in X$, $\lim_{x\to x_0} F(x)=F(x_0)$.

Soit $\varepsilon>0$. Comme $f$ est continue sur $[a,b]\times X$, d'après le lemme \ref{lemme1}, il existe $\delta>0$ tel que pour tout $t\in[a,b]$, tout $x\in X$, $\|x-x_0\|<\delta$ implique $|f(t,x)-f(t,x_0)|<\frac{\varepsilon}{b-a}$.

Pour tout $x\in X$ tel que $\|x-x_0\|<\delta$, nous avons alors :
\begin{align*}
 |F(x)-F(x_0)|
 &=\left|\int_a^b f(t,x)dt -\int_a^bf(t,x_0)dt\right|\\
 &\leq \int_a^b |f(t,x)-f(t,x_0)|dt\\
 &<\int_a^b \frac\varepsilon{b-a}dt=\varepsilon.
\end{align*}
Ainsi $\lim_{x\to x_0} F(x)=F(x_0)$ pour tout $x_0\in X$ donc $F$ est continue sur $X$.\qed
\begin{exemple}
 Soit $F$ la fonction définie pour $x\in\rr$ par $F(x)=\int_0^\pi \sin(x+t)e^{xt^2} dt$.
 On veut calculer $\lim_{\genfrac{}{}{0pt}{}{x\to 0}{x>0}} F(x)$.
 L'application $f:(t,x)\mapsto\sin(x+t)e^{xt^2}$ est continue sur $[0,\pi]\times\rr$ donc $F$ est continue sur $\rr$. En particulier, $\lim_{\genfrac{}{}{0pt}{}{x\to 0}{x>0}} F(x)=F(0)$ d'où
 \begin{align*}
  \lim_{\genfrac{}{}{0pt}{}{x\to 0}{x>0}} F(x)&=\int_0^\pi \sin tdt\\
  &= \left[-\cos t\right]_0^\pi\\
  &=2.
 \end{align*}
\end{exemple}

\section{Dérivabilité}

Nous rappelons le théorème des accroissements finis dont nous aurons besoin pour prouver le théorème de dérivation :
\begin{theoreme}[des accroissements finis]
 Soient $a$ et $b$ deux réels, $a<b$, $f:[a,b]\to\rr$ continue sur $[a,b]$ et dérivable sur $]a,b[$. Alors il existe $c\in]a,b[$ tel que 
 $$\frac{f(b)-f(a)}{b-a}=f'(c).$$
\end{theoreme}


 \begin{theoreme}[de dérivation des intégrales à paramètres définies]
Soient $I$ un intervalle de $\rr$,  $a,b\in\rr$, $a<b$, $f$ une fonction continue sur $[a,b]\times I$ telle que la dérivée partielle $\diffp{f}{x}$ existe et est continue sur $[a,b]\times I$. 

Alors la fonction $F:X\to \rr$ définie par $F(x)=\int_a^bf(t,x)dt$ est de classe $C^1$ sur $I$ et pour tout $x\in I$, $F'(x)=\int_a^b \frac{ \partial f}{ \partial x}(t,x)dt.$
  \end{theoreme}
\pr 
Soit $x_0\in I$. Nous commençons par montrer que $F$ est dérivable en $x_0$ et que $F'(x_0)=\int_a^b \frac{ \partial f}{ \partial x}(t,x_0)dt.$
Il s'agit donc de montrer que $\lim_{\genfrac{}{}{0pt}{}{x\to x_0}{x\in I}} \frac{F(x)-F(x_0)}{x-x_0}=\int_a^b\diffp{f}{x}(t,x_0)dt$ autrement dit que pour tout $\varepsilon>0$, il existe $\eta>0$ tel que pour tout $x\in I$, $|x-x_0|<\eta$ implique 
$$ \left|\frac{F(x)-F(x_0)}{x-x_0}-\int_a^b\diffp{f}{x}(t,x_0)dt\right|<\varepsilon.$$

Soit $\varepsilon >0$ et $x\in I$. Nous avons
\begin{align*}
 \left|\frac{F(x)-F(x_0)}{x-x_0}-\int_a^b \diffp f x (t,x_0)dt\right|
 &=\left|\int_a^b\frac{f(t,x)-f(t,x_0)}{x-x_0} dt
 -\int_a^b \diffp f x (t,x_0)dt\right|\\
 &\leq \int_a^b\left|\frac{f(t,x)-f(t,x_0)}{x-x_0}- \diffp f x (t,x_0)\right|dt.
\end{align*}
D'après le théorème des accroissements, pour tout $t, x, x_0$, il existe $\theta=\theta(t,x,x_0)$ tel que $|\theta -x_0|\leq |x-x_0|$ et 
$$\frac{f(t,x)-f(t,x_0)}{x-x_0}=\diffp{f}{x}(t,\theta).$$
D'autre part, comme $\diffp{f}{x}$ est continue sur $[a,b]\times I$, d'après le lemme \ref{lemme1}, il existe $\delta>0$ tel que pour tout $t\in[a,b]$ et tout $x'\in I$, $|x'-x_0|<\delta$ implique $\left|\diffp f x(t,x')-\diffp f x (t,x_0)\right|<\frac{\varepsilon}{b-a}$. Si $|x-x_0|<\delta$, nous avons $|\theta-x_0|<\delta,$ ce qui implique
\begin{align*}
 \left|\frac{f(t,x)-f(t,x_0)}{x-x_0}-\diffp f x (t,x_0)\right|
 &=\left|\diffp{f}{x}(t,\theta)-\diffp f x (t,x_0)\right|\\
 &<\frac{\varepsilon}{b-a}.
\end{align*}
Il s'ensuit alors 
\begin{align*}
 \left|\frac{F(x)-F(x_0)}{x-x_0}-\int_a^b \diffp f x (t,x_0)dt\right|
 &<\int_a^b \frac{\varepsilon}{b-a}dt=\varepsilon.
\end{align*}
Nous avons donc montré que  $\lim_{x\to x_0} \frac{F(x)-F(x_0)}{x-x_0}=\int_a^b\diffp{f}{x}(t,x_0)dt$. Donc $F$ est dérivable en tout $x_0\in I$ et $F'(x_0)=\int_a^b \diffp f x (t,x_0)dt$.

La fonction $\diffp f x$ étant continue sur $[a,b]\times I$, le théorème de continuité implique que $F'$ est continue et donc que $F$ est de classe $C^1$ sur $I$.\qed 
% \begin{exemple} Nous allons montrer que $\int_0^{+\infty} e^{-s^2}ds=\frac{\sqrt{\pi}}2$ en utilisant et en étudiant la fonction $F$ définie pour $x\in\rr$ par $F(x) = \int_0^1  \frac{e^{-x(1+t^2)}}{1+t^2}dt.$
% \begin{enumerate}[(i)]
%  \item L'application $f:(t,x)\mapsto \frac{e^{-x(1+t^2)}}{1+t^2}$ est définie et continue sur $[0,1]\times \rr$ car $\exp$ est continue et donc $f$ est le quotient de fonctions continues dont le dénominateur ne s'annule pas. Par conséquent, la fonction $F$ est bien définie et le  théorème de continuité implique que $F$ est continue sur $\rr$.
% \item Montrons que $\lim_{x\to +\infty} F(x)=0$. Pour tout $t\in\rr$, nous avons $1+t^2\geq 1$ donc pour tout $x>0$ et tout $t\in\rr$, $e^{-x(1+t^2)}\leq e^{-x}$. Ainsi pour tout $x>0$, il vient
% \begin{align*}
%  |F(x)|&=\int_0^1 \frac{e^{-x(1+t^2)}}{1+t^2}dt\\
%  &\leq e^{-x} \int_0^1 \frac1{1+t^2}dt\\
%  &\leq e^{-x} \frac\pi 4.
% \end{align*}
% On en déduit, avec le théorème des gendarmes, que $\lim_{x\to +\infty} F(x)=0$.
% \item  La fonction $f$ est de classe $C^\infty$ et $\diffp f x(t,x)=-e^{-x(1+t^2)}$. Par conséquent, d'après le théorème de dérivation, $F$ est de classe $C^1$ et $F'(x)=-\int_0^1e^{-x(1+t^2)} dt$.
% 
% \item  Soit $h(x)= F(x^2) + (u(x))^2$  où $u$ est définie pour $x\geq 0$ par $u(x)= \int_0^x e^{-s^2} ds$. On montre que $h$ est constante. La fonction $h$ est de classe $C^1$ car $F$ est de classe $C^1$ et car $u$ est la primitive d'une fonction continue. De plus, pour tout $x>0$ : 
% \begin{align*}
%  h'(x)
%  &=-2x\int_0^1 e^{-x^2(1+t^2)} dt +2e^{-x^2} \int_0^x e^{-s^2}ds\\
%  &=2xe^{-x^2} \int_0^1 e^{-x^2t^2} dt +2e^{-x^2} \int_0^x e^{-s^2}ds. 
% \end{align*}
% Dans l'intégrale $\int_0^x e^{-s^2}ds$, on fait le changement de variable $tx=s$ lorsque $x>0$ :
% $$\int_0^x e^{-s^2}ds=x\int_0^1 e^{-x^2t^2}dt$$
% d'où l'on déduit que $h'(x)=0$ pour tout $x>0$ et donc que $h$ est constante.
% 
% De plus 
% \begin{align*}
% h(0)&=F(0)\\
% &=\int_0^1 \frac1{1+t^2}dt\\
% &=\arctan(1)-\arctan(0)=\frac\pi4. 
% \end{align*}
% Ainsi pour tout $x\in\rr$, $h(x)=\frac\pi4$.
% \item  
% Enfin, l'intégrale généralisée $\int_0^{+\infty} e^{-s^2}dt $ converge car $0\leq e^{-s^2}\leq e^{-s}$ pour tout $s\geq 1$ et l'intégrale généralisée $\int_1^{+\infty} e^{-s} ds$ converge puisque $\lim_{x\to +\infty} \int_1^{x} e^{-s} ds=\lim_{x\to +\infty} e^{-1}-e^{-x} =e^{-1}$. Donc
% \begin{align*}
%  \frac\pi 4&= \lim_{x\to +\infty} h(x)\\
%  &=\left(\int_0^{+\infty} e^{-s^2}ds\right)^2
% \end{align*}
% d'où $\int_0^{+\infty} e^{-s^2}ds=\frac{\sqrt{\pi}}2$.
% \end{enumerate}
% 
% \end{exemple}
\section{Application : Théorème de Fubini}
\begin{theoreme}
 Soit $I=[\alpha,\beta]$, $J=[a,b]$ deux intervalles fermés, bornés. Soit $f:I\times J \to\rr$ une fonction continue. Alors 
 \begin{enumerate}[(i)]
  \item la fonction $F$ définie pour $x\in I$ par
  $$F(x)=\int_a^b f(x,t)dt$$
  est Riemann-intégrable sur $I$ ,
  \item la fonction $g$ définie pour $t\in J$ par
  $$G(t)=\int_\alpha^\beta f(x,t)dx$$
  est Riemann-intégrable sur $J$
  \item \label{iii} et on a 
  $$\int_\alpha^\beta F(x)dx=\int_a^b G(t)dt,$$
  autrement dit
  $$\int_\alpha^\beta \left(\int_a^b f(x,t)dt\right)dx
  =\int_a^b \left(\int_\alpha^\beta f(x,t)dx\right)dt,$$
  on peut donc échanger l'ordre d'intégration.
 \end{enumerate}
\end{theoreme}
\pr
Comme $f$ est continue, le théorème de continuité sous le signe intégrale implique que $F$ est continue sur $I$ et donc Riemann intégrable sur $I$. De même $G$ est continue et donc Riemann intégrable sur $J$.

Pour montrer (\ref{iii}), on introduit la fonction $h$ définie pour tout  $(x,t)\in I\times J$ par
$$h(x,t)=\int_\alpha^x f(y,t)dy$$
et la fonction $H$ définie pour tout $x\in I$ par
$$H(x)=\int_a^b h(x,t)dt.$$
Pour $t$ fixé, $h(\cdot,t)$ est donc la primitive de $f(\cdot,t)$ qui s'annule en $\alpha$. Pourvu que l'on sache justfier les calculs suivants, nous aurons montré le théorème :
\begin{align*}
H'(x)
&= \int_a^b\diffp h x(x,t)dt, \text{ si $H$ est dérivable et si }H'(x)=\int_a^b \diffp h x(x,t)dt,\\
&= \int_a^b f(x,t)dt, \text{ si } \diffp h x (x,t)=f(x,t),
\end{align*}
d'où
\begin{align*}
\int_\alpha^\beta  \left(\int_a^b f(x,t)dt\right) dx
&= \int_\alpha^\beta H'(x)dx\\
&= H(\beta)-H(\alpha) \\
&= H(\beta) \text{ pourvu que }H(\alpha)=0\\
&= \int_a^b h(\beta,t)dt\\
&=\int_a^b \left(\int_\alpha^\beta f(y,t)dy\right)dt.
\end{align*}


On justifie maintenant ces calculs :
\begin{itemize}
 \item La fonction $f$ étant continue, elle est Riemann intégrable et donc $h$ est bien définie.
 \item On montre que la fonction $h$ est continue sur $I\times J$, cela montrera qu'elle est Riemann intégrable et donc que $H$ est bien définie elle aussi.
 Soit $(x_0,t_0)\in I\times J$ et soit $\varepsilon>0$. Pour tout $(x,t)\in I\times J$, on a:
 \begin{align*}
  h(x,t)-h(x_0,t_0)
  &=\int_\alpha^x f(y,t)dy-\int_\alpha^{x_0} f(y,t_0)dy\\
  &=\int_\alpha^x f(y,t)dy-\int_\alpha^{x} f(y,t_0)dy
  +\int_\alpha^x f(y,t_0)dy-\int_\alpha^{x_0} f(y,t_0)dy\\
  &=\int_\alpha^x (f(y,t)- f(y,t_0))dy +\int_{x_0}^x f(y,t_0)dy.
 \end{align*}
Comme $f$ est continue sur $I\times J$ qui est compact, $f$ est bornée sur $I\times J$. Il existe $M\geq 0$ tel que pour tout $(x,t)\in I\times J$, $|f(x,t)|\leq M$. On en déduit que 
 $$\left|\int_{x_0}^x f(y,t_0)dy\right|\leq (x-x_0)M.$$
 Ainsi, si $|x-x_0|<\frac{\varepsilon}{2M},$ on a  $$\left|\int_{x_0}^x f(y,t_0)dy\right|\leq \frac\varepsilon2.$$
 
Comme $f$ est continue sur le compact  $I\times J$, $f$ est uniformément continue sur $I\times J$. Il existe donc $\eta >0$ tel que pour tout $(y,t), (y',t')\in I\times J$, si $|y-y'|+|t-t'|<\eta$, alors $|f(y,t)-f(y',t')|<\frac\varepsilon{2(\beta-\alpha)}$
On en déduit que si $|t-t_0|<\eta$, alors 
\begin{align*}
\left|\int_\alpha^x (f(y,t)- f(y,t_0))dy\right|
&\leq  \int_\alpha^x \frac\varepsilon{2(\beta-\alpha)}dy\\
&\leq  \frac{\varepsilon}2 \frac{x-\alpha}{\beta-\alpha}\\
&\leq \frac\varepsilon2.
\end{align*}
Ainsi, pour $\delta= \min\left(\frac{\varepsilon}{2M},\eta\right)$ dès que $|x-x_0|<\delta$ et $|t-t_0|<\delta$, on a $|h(x,t)-h(x_0,t_0)|<\varepsilon$, autrement dit, $\lim_{(x,t)\to(x_0,t_0)}h(x,t)=h(x_0,t_0)$.
\item $h$ est dérivable par rapport à $x$ sur $I$ et pour tout $(x,t)\in I\times J$, $\diffp h x(x,t)=f(x,t)$. En particulier, $\diffp h x$ est continue sur $I\times J$.

\item Par définition de $h$, pour tout $t\in J$, l'application $x\mapsto \int_\alpha^xf(y,t)dy$ est la primitive de $x\mapsto f(x,t)$ qui s'annule en $\alpha$. Par conséquent $h$ est dérivable par rapport à $x$ et $\diffp h x (x,t)=f(x,t)$ donc $\diffp h x$ est continue.

\item Comme $h$ est continue, dérivable par rapport à $x$ et comme $\diffp h x$ est continue, d'après le théorème de dérivation sous le signe intégrale, $H$ est de classe $C^1$ et $H'(x)=\int_a^b \diffp h x(x,t)dt=\int_a^bf(x,t)dt.$
 \item Enfin, $H(\alpha)=\int_a^b \left(\int_\alpha^\alpha f(y,t)dy\right)dt=0$.
\end{itemize}
\qed
\begin{exemple}
Calculer  $I=\int_1^2\left(\int_0^2 ye^{xy}dy\right)dx.$

La fonction $(x,y)\mapsto ye^{xy}$ est continue sur $[1,2]\times [0,2]$. Le théorème de Fubini implique alors que
\begin{align*}
 I
 &=\int_0^2\left(\int_1^2 ye^{xy}dx\right)dy\\
 &=\int_0^2\left[e^{xy}\right]_{x=1}^{x=2}dy\\
 &=\int_0^2\left(e^{2y}-e^y\right)dy\\
 &=\left[\frac12 e^{2y}-e^y\right]_0^2\\
 &=\frac12 e^4-e^2+\frac12.
\end{align*}
\end{exemple}

\section{Intégrale à paramètres dont les bornes dépendent d'un paramètre}
\begin{theoreme}\label{davant}
Soient $I$ et $J$ deux intervalles fermés et bornés de $\rr$ et $f:I\times J\to\rr$, $a,b:I\to J$ trois fonctions continues.

Alors la fonction $F$ définie sur $I$ par 
$$F(x)=\int_{a(x)}^{b(x)} f(x,t)dt$$
est de continue sur $I$.
\end{theoreme}
\pr On considère $\phi: I\times J\times J \to\rr$ définie par 
$$\phi(x,u,v)=\int_u^v f(x,t)dt.$$
Comme $f$ est continue, $f$ est Riemann intégrable sur $[u,v]$ (ou $[v,u]$) et donc $\phi$ est bien définie. On montre que $\phi$ est continue. Soit $(x_0,u_0,v_0), (x,u,v)\in I\times J\times J$ et soit $\varepsilon>0$. On a
\begin{align*}
 \phi(x,u,v)-\phi(x_0,u_0,v_0)
 &=\int_u^v f(x,t)dt-\int_{u_0}^{v_0} f(x_0,t)dt\\
 &=\int_{u}^{u_0} f(x,t)dt+\int_{v_0}^{v} f(x,t)dt 
 +\int_{u_0}^{v_0} (f(x,t)-f(x_0,t)dt.
\end{align*}
Comme $f$ est continue sur le compact $I\times J$, elle est bornée et uniformément continue sur $I\times J$. Soit $M\geq 0$ tel que pour tout $(x,t)\in I\times J$, on ait $|f(x,t)|\leq M$. Alors on a $\left|\int_{u}^{u_0} f(x,t)dt\right|\leq M|u-u_0|$. Ainsi, si $|u-u_0|<\frac\varepsilon{3M}$, on a $\left|\int_{u}^{u_0} f(x,t)dt\right|\leq \frac{\varepsilon}3$.

De même, si $|v-v_0|<\frac\varepsilon{3M}$, on a $\left|\int_{v}^{v_0} f(x,t)dt\right|\leq \frac{\varepsilon}3$.

Enfin, si $u_0=v_0$ alors $\int_{u_0}^{v_0} (f(x,t)-f(x_0,t)dt=0<\frac\varepsilon3$. Sinon, 
par l'uniforme continuité de $f$, il existe $\eta>0$ tel que pour tout $(y,t), (y',t')\in I\times J$, si $|y-y'|<\eta$ et $|t-t'|<\eta$, alors
$|f(y,t)-f(y',t')|<\frac{\varepsilon}{3|u_0-v_0|}$.
Ainsi si $|t-t_0|<\eta$, $\left|\int_{u_0}^{v_0} (f(x,t)-f(x_0,t)dt\right|\leq \frac\varepsilon3$.

On en déduit finalement que si $|u-u_0|<\frac \varepsilon {3M}$, $|v-v_0|<\frac \varepsilon {3M}$ et $|t-t_0|<\eta$, on a $|\phi(x,u,v)-\phi(x_0,u_0,v_0)|<\varepsilon$ ce qui montre que $\lim_{(x,u,v)\to (x_0,u_0,v_0)} \phi(x,u,v)=\phi(x_0,u_0,v_0)$, i.e.~ $\phi$ est continue en $(x_0,u_0,v_0)$ et donc sur $I\times J\times J$. Par suite $F:x\mapsto \phi(x,a(x),b(x))$ est la composée de fonctions continues et est donc continue.\qed

\begin{theoreme}
Soient $I$ et $J$ deux intervalles fermés et bornés de $\rr$ et $f:I\times J\to\rr$, $a,b:I\to J$ trois fonctions telles que :
\begin{enumerate}[(i)]
 \item $a$ et $b$ sont de classe $C^1$,
 \item $f$ est continue;
 \item $\diffp f x$ existe et est continue sur $I\times J$.
\end{enumerate}
Alors la fonction $F$ définie sur $I$ par 
$$F(x)=\int_{a(x)}^{b(x)} f(x,t)dt$$
est de classe $C^1$ sur $I$ et pour tout $x\in I$, on a
$$F'(x)=b'(x) f(x,b(x))-a'(x) f(x,a(x))+\int_{a(x)}^{b(x)} \diffp f x (x,t) dt.$$
\end{theoreme}
\pr On considère de nouveau $\phi: I\times J\times J \to\rr$ définie par 
$$\phi(x,u,v)=\int_u^v f(x,t)dt.$$
Pour $u$ et $x$ fixés, $\phi$ est dérivable par rapport à $v$ puisque c'est la primitive de $t\mapsto f(x,t)$ qui s'annule en $u$ et on a $\diffp \phi v(x,u,v)= f(x,v)$. De même $\diffp \phi u$ existe et $\diffp \phi u(x,u,v)=-f(x,u)$.

Lorsque $u$ et $v$ sont fixés, comme $f$ est continue sur $I\times[u,v]$, dérivable par rapport à $x$ et $\diffp f x$ est continue sur $I\times [u,v]$, le théorème de dérivation sous le signe intégrale implique que $\phi$ est dérivable par rapport à $x$ et que 
$$\diffp \phi x(x,u,v)=\int_u^v \diffp f x(x,t)dt.$$

On en déduit que $F: x\mapsto \phi(x,a(x),b(x))$ est dérivable et que 
\begin{align*}
F'(x)&=b'(x) \diffp \phi u (x,a(x),b(x)) +a'(x) \diffp \phi v (x,a(x),b(x)) +\diffp \phi x (x,a(x),b(x))\\
&=b'(x) f(x,b(x))-a'(x) f(x,a(x))+\int_{a(x)}^{b(x)} \diffp f x (x,t) dt. 
\end{align*}
Comme $ \diffp f x$ est continue, d'après le théorème \ref{davant}, $(x,u,v)\mapsto \int_u^v \diffp f x (x,t)dt$ est continue et donc finalement $F'$ est continue.\qed
\end{document}
