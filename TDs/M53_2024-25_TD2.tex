\documentclass[a4paper,10pt,reqno]{amsart}
\usepackage{lille}
\input{m53}

\lilleset{titre=TD2 - Intégrales définies dépendant d'un paramètre}

\begin{document}

% -----------------------------------------------
\begin{exo}

  Pour $x\in\mathbb{R}$, on pose
  $\displaystyle
    F(x)=\int_0^1 \cos(tx)\,dt.
  $
  \begin{enumerate}
    \item Montrer que $F$ est bien définie, continue et paire sur $\mathbb{R}$.
    \item Montrer que $F$ est dérivable sur $\mathbb{R}$ et calculer sa dérivée.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}[.49]

  Soit
  $\displaystyle
    F(x)=\int_0^1 t^{3}e^{xt}\,dt$, $x\in\mathbb{R}.
  $
  Montrer que $F$ est de classe $C^1$ sur $\mathbb{R}$ et calculer $F'(0)$.
\end{exo}

% -----------------------------------------------
\begin{exo}

  Pour $x\in\mathbb{R}$ on pose $F(x)=\displaystyle\int_0^1 \dfrac{dt}{(1+t^2)(1+t^2x^2)}.$
  \begin{enumerate}
    \item Montrer que $F$ est bien définie et continue sur $\mathbb{R}$.
    \item Par une décomposition en éléments simples, calculer $F(x)$ et en déduire la valeur de $\displaystyle\int_0^1 \dfrac{dt}{(1+t^2)^2}$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}[.49]

  Pour $x\in\mathbb{R}$, on pose
  $\displaystyle
    F(x)=\int_0^1 e^{-xt}\frac{\sin(t)}{t}\,dt.
  $
  Montrer que $F$ est bien définie et dérivable sur $\mathbb{R}$.
\end{exo}

% -----------------------------------------------
\begin{exo}

  Pour $x>0$, on pose
  $\displaystyle
    F(x)=\int_0^1 \frac{dt}{x^2+t^2}.
  $
  \begin{enumerate}
    \item Montrer que $F$ est bien définie, de classe $C^1$ sur $]0,+\infty[$, et pour $x>0$, exprimer $F'(x)$ sous la forme d'une intégrale (qu'on ne cherchera pas à calculer).
    \item Calculer $F(x)$ directement par un calcul de primitive.
    \item En déduire la valeur de
    $\displaystyle
      \int_0^1 \frac{dt}{(1+t^2)^2}.
    $
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Pour $x\in ]-1,1[$, on pose
  \[
    F(x)=\displaystyle\int_0^{2\pi}\ln(1-2x\cos(t)+x^2)\,dt.
  \]
  \begin{enumerate}
    \item Montrer que $F$ est bien définie sur $]-1,1[$ et que pour $x\in ]-1,1[$, on a
    \[
      F(x)=2\int_0^\pi \ln(1-2x\cos(t)+x^2)\,dt.
    \]
    \item Montrer que $F$ est dérivable sur $]-1,1[$.
    \item En posant $u=\tan(t/2)$, montrer que pour tout $x\in ]-1,1[$, on a $F'(x)=0$.
    \item En déduire la valeur de $F(x)$ pour $x\in ]-1,1[$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Pour $x\in\mathbb{R}$, on pose
  \[
    F(x)=\int_0^1\frac{e^{-tx}}{1+t^2}\,dt.
  \]
  \begin{enumerate}
    \item Justifier que $F$ est bien définie sur $\mathbb{R}$.
    \item Montrer que $F$ est une fonction de classe $C^2$ sur $\mathbb{R}$.
    \item Calculer $F(0)$ et $F'(0)$.
    \item Montrer que $F$ est strictement décroissante sur $\mathbb{R}$ et convexe.
    \item Montrer que $\lim_{x\to+\infty}F(x)=0$ et $\lim_{x\to -\infty}F(x)=+\infty$.
    \item Donner l'allure du graphe de $F$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Soit
  $\displaystyle
    F(x) = \int_0^{\frac{\pi}{2}}\ln(1+x\sin^2(t))\,dt$, $x>-1.
  $
  \begin{enumerate}
    \item Montrer que $F$ est définie et dérivable sur $]-1,+\infty[$. Calculer $F'(0)$.
    \item A l'aide du changement de variable $u=\tan(t)$, montrer que pour tout $x>-1$, on a
    \[
      F'(x)=\frac{\pi}{2}\frac{1}{\sqrt{1+x}(\sqrt{1+x}+1)}.
    \]
    \item En déduire finalement la valeur de $F(x)$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Pour $x>0$, on définit
  $\displaystyle
    f(x)=\int_0^{\frac\pi2} \frac{\cos(t)}{t+x} dt.
  $
  \begin{enumerate}
    \item Justifier que $f$ est de classe $C^1$ sur $]0,+\infty[$ et étudier les variations de $f$.
    \item Déterminer $\lim_{x\to +\infty} f(x)$.
    \item En utilisant $1-\frac{t^2}2\leq \cos(t)\leq 1$, $t\in[0,\frac\pi 2]$, démontrer que $f(x)\sim_{x\to 0^+}-\ln x$.
    \item Déterminer un équivalent de $f$ en $+\infty$.\\
    \begin{indication}
      on pourra calculer $xf(x)-1$.
    \end{indication}
\end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Soit $f:[0,1]\longrightarrow\mathbb{R}$ une fonction continue. On définit
  $\displaystyle
    F(x)=\int_0^1 \frac{xf(t)}{x^2+t^2}\,dt,\ x>0.
  $
  \begin{enumerate}
    \item Montrer que $F$ est continue sur $]0,+\infty[$.
    \item Montrer que
    $\displaystyle
      \lim_{x\to 0^+}\int_0^1 \frac{x}{x^2+t^2}\,dt=\dfrac{\pi}{2}.
    $
    \item Montrer que
    $\displaystyle
      \lim_{x\to 0^+}\left(F(x)-f(0)\int_0^1 \frac{x}{x^2+t^2}\,dt\right)=0.
    $\\
    \begin{indication}
      On pourra écrire que
      $\displaystyle
        F(x)-f(0)\int_0^1 \frac{x}{x^2+t^2}\,dt=\int_0^1 \frac{x}{x^2+t^2}(f(t)-f(0))\,dt
      $
      et découper l'intégrale en deux, sur $[0,\delta]\cup[\delta,1]$. Pour la première intégrale, on pourra alors utiliser la continuité de $f$ en $0$.
    \end{indication}
    \item En déduire que $\lim_{x\to 0^+}F(x)=\dfrac{\pi}{2}f(0)$.
    \item Pourquoi ne peut-on pas appliquer le théorème de continuité des intégrales à paramètres pour répondre à la question précédente?
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}
  \begin{enumerate}
    \item Soit $\varphi$ une fonction définie sur un voisinage de $0$, à valeurs strictement positive et dérivable en $0$. Supposons en plus que $\varphi(0)=1$. Déterminer $\lim_{x\to 0}(\varphi(x))^{1/x}$.
    \item Soit $f:[0,1]\longrightarrow\mathbb{R}$ continue et supposons que pour tout $t\in [0,1]$, $f(t)>0$. Posons
    \[
      I(x)=\int_0^1 f(t)^x\,dt.
    \]
    Montrer que $I$ est bien définie et dérivable sur $\mathbb{R}$.
    \item En déduire que
    \[
      \lim_{x\to 0}\left(\int_0^1 f(t)^x\,dt\right)^{1/x}=\exp\left(\int_0^1 \log(f(t))\,dt\right).
    \]
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Soit $f:\mathbb{R}\longrightarrow\mathbb{R}$ une fonction de classe $C^\infty$.
  \begin{enumerate}
    \item On suppose que $f(0)=0$ et on pose $g(x)=f(x)/x$, $x\neq 0$. Justifier que pour $x\neq 0$, on a
    \[
      g(x)=\int_0^1 f'(tx)\,dt.
    \]
    En déduire que $g$ se prolonge en une fonction $C^\infty$ sur $\mathbb{R}$.
    \item On suppose maintenant que $f(0)=f'(0)=\dots=f^{(n-1)}(0)=0$. On pose maintenant $g(x)=f(x)/x^n$, $x\neq 0$. Justifier que $g$ se prolonge en une fonction de classe $C^\infty$ sur $\mathbb{R}$.\\
    \begin{indication}
    on pourra utiliser la formule de Taylor avec reste intégral.
    \end{indication}
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Soit $f:\mathbb{R}\longrightarrow\mathbb{R}$ une fonction continue. Posons
  \[
    F(x)=\int_0^x \sin(x-t)\,f(t)\,dt,\qquad x\in\mathbb{R}.
  \]
  \begin{enumerate}
    \item En utilisant le théorème de dérivabilité des intégrales à paramètres, montrer que $F$ est de classe $C^2$ et satisfait $F''(x)+F(x)=f(x)$, $x\in\mathbb{R}$.
    \item Comment peut-on retrouver ce résultat?
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Pour $x>0$, on pose
  \[
    F(x)=\int_1^x \frac{\ln(x+t)}{t}\,dt
      \quad\text{et}\quad
    G(x)=\frac{1}{2}(\ln x)^2+\int_1^x\frac{\ln(1+t)}{t}\,dt.
  \]
  \begin{enumerate}
    \item Montrer que $F$ et $G$ sont bien définies et dérivables sur $]0,\infty[$ et vérifier que $F'(x)=G'(x)$, $x>0$.
    \item En déduire que pour tout $x>0$, on a
    \[
      \int_1^x \frac{\ln(x+t)}{t}\,dt=\frac{1}{2}(\ln x)^2+\int_1^x\frac{\ln(1+t)}{t}\,dt.
    \]
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Pour $x\in I=[0,+\infty[$, on pose
  \[
    F(x)=\int_0^1 \frac{e^{-x^2(t^2+1)}}{t^2+1}\,dt
      \quad\text{et}\quad
    G(x)=\left(\int_0^x e^{-t^2}\,dt\right)^2.
  \]
  \begin{enumerate}
    \item Montrer que $F$ est définie, continue et dérivable sur $I$. Exprimer sa dérivée sous forme d'une intégrale (qu'on ne cherchera pas à calculer).
    \item Justifier que $G$ est définie, continue et dérivable sur $I$ et montrer que
    \[
      F'(x)+G'(x)=0,\quad\forall x\in I.
    \]
    \item En déduire que, $\forall x\in I$, $F(x)+G(x)=\dfrac{\pi}{4}$.
    \item Montrer que $\lim_{x\to +\infty}F(x)=0$.\\
    \begin{indication}
      On pourra remarquer que $|F(x)|\leq e^{-x^2}$, $x\in\mathbb{R}$.
    \end{indication}
    \item En déduire que $\int_0^{+\infty}e^{-t^2}\,dt=\dfrac{\sqrt{\pi}}{2}$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Soit $F(x)=\dfrac{1}{\pi}\displaystyle\int_0^\pi \frac{t\sin(t)}{1-x\cos(t)}\,dt$.
  \begin{enumerate}
    \item Montrer que $F$ est continue et dérivable sur $[0,1[$. Exprimer $F'(x)$ sous forme d'une intégrale (qu'on ne cherchera pas à calculer).
    \item Montrer que
    \[
      xF'(x)=-F(x)+\frac{1}{1+x}+\frac{1}{\pi}\int_0^\pi \frac{\cos(t)}{1-x\cos(t)}\,dt.
    \]
    \begin{indication}
      On effectuera une intégration par partie dans l'intégrale donnant $xF'(x)$.
    \end{indication}
    \item Montrer que
    \[
      \frac{1}{\pi}\int_0^\pi \frac{\cos(t)}{1-x\cos(t)}\,dt=-\frac{1}{x}+\frac{1}{x\sqrt{1-x^2}},
    \]
    et en déduire que $F$ est solution sur $]0,1[$ de l'équation différentielle suivante :
    \begin{gather}
      x y'+y=-\frac{1}{x}+\frac{1}{1+x}+\frac{1}{x\sqrt{1-x^2}}.\label{eq:E}\tag{E}
    \end{gather}
    \item Calculer une primitive sur $]0,1[$ de $x\longmapsto \dfrac{1}{x\sqrt{1-x^2}}$.\\
    \begin{indication}
      On pourra effectuer le changement de variable $x=\sin(t)$, $t\in ]0,\frac{\pi}{2}[$.
    \end{indication}
    \item Résoudre \eqref{eq:E} sur $]0,1[$ et montrer qu'il existe une et seule solution qui se prolonge par continuité sur $[0,1]$.
    \item Montrer que l'intégrale (généralisée) $\displaystyle\int_0^\pi \frac{t\sin(t)}{1-\cos(t)}\,dt$ converge.  On notera $I$ sa valeur.
    \item Montrer que $\lim_{x\to 1^-}F(x)=\dfrac{I}{\pi}$.
    \begin{indication}
      On pourra découper l'intégrale en deux, sur $[0,\varepsilon]$ puis $[\varepsilon,\pi]$.
    \end{indication}
    \item En déduire la valeur de $I$.
  \end{enumerate}
\end{exo}

\end{document}
