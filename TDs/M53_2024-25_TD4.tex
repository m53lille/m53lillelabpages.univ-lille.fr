\documentclass[a4paper,10pt,reqno]{amsart}
\usepackage{lille}
\input{m53}

\lilleset{titre=TD4 - Séries de Fourier}

\begin{document}

% -----------------------------------------------
\begin{exo}

  Soit $f$ la fonction paire, $2\pi$-périodique et définie par $f(x)=x$, $x\in [0,\pi]$.
  \begin{enumerate}
    \item Déterminer la série de Fourier de $f$.
    \item Etudier la convergence de cette série de Fourier.
    \item En déduire les valeurs des séries suivantes :
    \begin{center}
      \begin{enumerate*}[\quad\bf1.]
        \item $\displaystyle \sum_{n=0}^{+\infty}\frac{1}{(2n+1)^2},$
        \item $\displaystyle \sum_{n=0}^{+\infty}\frac{1}{(2n+1)^4},$
        \item $\displaystyle \sum_{n=1}^{+\infty}\frac{1}{n^2},$
        \item $\displaystyle \sum_{n=1}^{+\infty}\frac{1}{n^4}.$
      \end{enumerate*}
    \end{center}
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  On considère la fonction $2\pi$-périodique $f:\mathbb{R}\longrightarrow \mathbb{R}$ définie par
  \[
    f(x)=\begin{cases}
      -1 & \mbox{si }x\in ]-\pi,0[        \\
       0 & \mbox{si }x=0 \mbox{ ou }x=\pi \\
       1 & \mbox{si }x\in ]0,\pi[.
    \end{cases}
  \]
  \begin{enumerate}
  \item Déterminer la série de Fourier de $f$.
  \item Etudier la convergence de cette série de Fourier.
  \item En déduire la valeur de $\sum_{k\geq 0} \frac{(-1)^{k}}{2k+1}$.
  \item Ecrire l'identité de Parseval pour cette série de Fourier.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Reprendre l'exercice précédent avec les fonctions $2\pi$-périodiques définies par :
  \begin{enumerate}
    \item $g(x)=\frac{x}{2}$ si $x\in ]-\pi,\pi[$ et $g(\pi)=0$ et la série de terme général $\frac1{n^2}$.
    \item $h(x)=|\sin(x)|$ pour $x\in [-\pi,\pi[$ et la série de terme général $\frac1{4n^2-1}$.
    \item $k(x)=|x|(\pi-|x|)$ pour $x\in [-\pi,\pi[$ et les séries de terme général $\frac1{n^2}$, $\frac{(-1)^n}{n^2}$ et $\frac1{n^4}$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  On considère la fonction $f$ de période $2\pi$, définie par $f(x)=\ch(x)$, $x\in [-\pi,\pi[$.
  \begin{enumerate}
    \item Déterminer pour $n\in\mathbb{Z}$, $c_n(f)$, puis pour $n\in\mathbb{N}$, $a_n(f)$ et $b_n(f)$.
    \item Etudier la convergence de la série de Fourier de $f$.
    \item En déduire les valeurs de $\sum_{n=1}^{+\infty}\frac{(-1)^n}{n^2+1}$ et $\sum_{n=1}^{+\infty}\frac{1}{n^2+1}$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Soit $f$ la fonction $2\pi$-périodique définie par $f(x)=\frac{\pi-x}{2}$, $x\in [0,2\pi[$.
  \begin{enumerate}
    \item Calculer les. coefficients de Fourier de $f$.
    \item Etudier la convergence de la série de Fourier de $f$.
    \item Calculer $\sum_{n=0}^{+\infty}\frac{(-1)^n}{2n+1}$ et $\sum_{n=1}^{+\infty}\frac{1}{n^2}$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Soit $f:[-1,1]\longrightarrow\mathbb{R}$ définie par $f(x)=x(1-x^2)$.
  \begin{enumerate}
  \item Montrer qu'il existe une suite de réels $\suite[n\geq 1]{b_n}$ telle que pour tout $x\in [-1,1]$, on a
  \[
    f(x)=\sum_{n=1}^{+\infty}b_n \sin(n\pi x).
  \]
  \item Calculer les coefficients $\suite[n\geq 1]{b_n}$.
  \item En déduire les valeurs de
  \[
    \sum_{p=0}^{+\infty}\frac{1}{(2p+1)^4}\quad\mbox{et}\quad \sum_{n=1}^{+\infty}\frac{1}{n^6}.
  \]
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Soit $g$ la fonction $2\pi$-périodique, paire, égale à $g(x)=\pi$ si $0\leq x\leq 1$ et $g(x)=0$ si $1<x\leq \pi$.
  \begin{enumerate}
  \item Déterminer la série de Fourier de $g$ et discuter sa convergence.
  \item Montrer que
  \[
    \sum_{n=1}^{+\infty}\frac{\sin(n)}{n}=\sum_{n=1}^{+\infty}\left(\frac{\sin(n)}{n}\right)^2.
  \]
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}{\emph{(Le développement eulérien du sinus)}}

  Soit $\alpha\in\mathbb{R}\setminus\mathbb{Z}$. On désigne par $f_\alpha$ la fonction $2\pi$-périodique sur $\mathbb{R}$ telle que
  \[
    \forall t\in ]-\pi,\pi],\qquad f_\alpha(t)=\cos(\alpha t).
  \]
  \begin{enumerate}
  \item Calculer la série de Fourier de $f_\alpha$. En déduire que
  \[
    \forall t\in\mathbb{R}\setminus\pi\mathbb{Z},\qquad \cotan(t)=\frac{1}{t}+2t\sum_{n=1}^{+\infty}\frac{1}{t^2-n^2\pi^2}.
  \]
  \item Fixons $x\in ]0,\pi[$ et soit $f:[0,x]\longrightarrow \mathbb{R}$ définie par
  \[
    f(t)=\begin{cases}
      \cotan(t)-\frac{1}{t} & \text{si } 0<t\leq x \\
      0                     & \text{si }t=0.
    \end{cases}
  \]
  Vérifier que $f$ est continue sur $[0,x]$ et montrer que
  \[
    \int_0^x f(t)\,dt=\sum_{n=1}^{+\infty}\log\left(1-\frac{x^2}{n^2\pi^2}\right).
  \]
  \item En déduire que
  \[
    \forall t\in ]-\pi,\pi[,\qquad \sin(t)=t\prod_{n=1}^{+\infty} \left(1-\frac{t^2}{n^2\pi^2}\right),
  \]
  où l'égalité ci-dessus signifie que la suite $t\prod_{n=1}^N\left(1-\frac{t^2}{n^2\pi^2}\right)$ converge vers $\sin(t)$ quand $N\to+\infty$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Soit $f:\mathbb{R}\longrightarrow \mathbb{C}$ définie par $f(t)=e^{e^{it}}$, $t\in\mathbb{R}$.
  \begin{enumerate}
  \item Justifier que $f$ est égale à la somme de sa série de Fourier.
  \item Montrer que pour tout $t\in\mathbb{R}$, on a
  \[
    f(t)=\sum_{k=0}^{+\infty}\frac{e^{ikt}}{k!}.
  \]
  \item En déduire les coefficients de Fourier $c_n$ de $f$, pour $n\in\mathbb{Z}$.
  \item Montrer que
  \[
    \int_0^{2\pi} e^{2\cos(t)}\,dt=2\pi\sum_{n=0}^{+\infty}\frac{1}{(n!)^2}.
  \]
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}{\emph{(Inégalité de Wirtinger)}}\

  \begin{enumerate}
    \item Soient $(A,B)\in\mathbb{C}^2$ et $g:\mathbb{R}\longrightarrow\mathbb{C}$ définie par $g(t)=Ae^{it}+B e^{-it}$. Montrer que
    \[
      \int_0^{2\pi}|g(t)|^2\,dt=\int_0^{2\pi}|g'(t)|^2\,dt.
    \]
    \item Soit $f:\mathbb{R}\longrightarrow \mathbb{C}$ une fonction de classe $C^1$, $2\pi$-périodique et telle que $\int_0^{2\pi}f(t)\,dt=0$. Montrer que
    \[
      \int_0^{2\pi}|f(t)|^2\,dt\leq \int_0^{2\pi}|f'(t)|^2\,dt.
    \]
    \item Dans quel cas y-a-t-il égalité?
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  \begin{enumerate}
    \item Existe-t-il une suite réelle $\suite[n\geq 0]{a_n}$ telle que pour tout $x\in [0,\pi]$, $\sin(x)=\sum_{n=0}^{+\infty}a_n\cos(nx)$?
    \item Existe-t-il une suite réelle $\suite[n\geq 0]{b_n}$ telle que pour tout $x\in ]0,\pi[$, $\cos(x)=\sum_{n=0}^{+\infty}b_n\sin(nx)$?
    \item Existe-t-il une suite réelle $\suite[n\geq 0]{c_n}$ telle que pour tout $x\in [0,2\pi]$, $\sin(x)=\sum_{n=0}^{+\infty}c_n\cos(nx)$?
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  \begin{enumerate}
    \item La série trigonométrique $\displaystyle\sum_{n=1}^{+\infty}\frac{\sin(nx)}{\sqrt{n}}$ est-elle la série de Fourier d'une fonction continue par morceaux $2\pi$-périodique?
    \item La série trigonométrique $\displaystyle\sum_{n=1}^{+\infty}\frac{\cos(nx)}{n}$ est-elle la série de Fourier d'une fonction $C^1$ par morceaux $2\pi$-périodique?
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}{\emph{(Equations différentielles et séries de Fourier)}}

  On considère l'équation différentielle
  \begin{gather}
    y''(t)-y(t)e^{it}=0.\tag{E}\label{eq:E}
  \end{gather}
  \begin{enumerate}
    \item
    \begin{enumerate}
      \item Montrer que la série trigonométrique
      \[
      \sum_{n\geq 0}\frac{(-1)^n}{(n!)^2}e^{int}
      \]
      converge pour tout $t\in\mathbb{R}$, et que sa somme $f$ est $2\pi$-périodique.
      \item Montrer que $f$ est de classe $C^2$ sur $\mathbb{R}$ et que $f$ est solution de l'équation différentielle \eqref{eq:E}.
    \end{enumerate}
    \item Soit $g:\mathbb{R}\longrightarrow \mathbb{C}$ une solution $2\pi$-périodique de classe $C^2$ de l'équation \eqref{eq:E}.
    \begin{enumerate}
      \item Pour $n\in\mathbb{Z}$, en utilisant que $g$ est solution de \eqref{eq:E}, exprimer $c_n(g'')$ en fonction de $c_{n-1}(g)$.
      \item En utilisant que $g$ est de classe $C^2$, exprimer $c_n(g'')$ en fonction de $c_n(g)$, pour $n\in\mathbb{Z}$. En déduire, pour tout $n\in\mathbb{Z}$ une relation entre $c_n(g)$ et $c_{n-1}(g)$.
      \item Calculer, pour $n\in\mathbb{Z}$, $c_n(g)$ en fonction de $c_0(g)$.
      \item En déduire l'espace vectoriel des solutions complexes $2\pi$-périodiques de l'équation différentielle \eqref{eq:E}.
    \end{enumerate}
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}{\emph{(Equations différentielles et séries de Fourier)}}

  On considère l'équation différentielle
  \[
    (E_{a,b}) \qquad \qquad y''(t)+(a+be^{2it})y(t)=0,
  \]
  avec $a,b$ deux nombres complexes.
  \begin{enumerate}
    \item On suppose dans cette question que $a$ est réel et $b=0$. Résoudre $(E_{a,0})$. L'équation $(E_{a,0})$ admet-elle des solutions non nulles $2\pi$-périodiques?
    \item Soit $f$ une fonction indéfiniment dérivable et  $2\pi$-périodique de $\mathbb{R}$ dans $\mathbb{R}$. Montrer que, pour tout entier $k$ strictement positif, on a lorsque $n$ tend vers $+\infty$ :
    \[
      c_n(f)=o\left(\frac{1}{n^k}\right)\quad \text{et}\quad c_{-n}(f)=o\left(\frac{1}{n^k}\right).
    \]
    \item
    \begin{enumerate}
      \item Montrer que toute solution de $(E_{a,b})$, $2\pi$-périodique, est indéfiniment dérivable, développable en série de Fourier ainsi que ses dérivées.
      \item Soit $g$ la fonction définie de $\mathbb{R}$ dans $\mathbb{C}$ par $g(t)=(a+be^{2it})f(t)$. Pour tout entier $n$, calculer $c_n(g)$ en fonction de $c_n(f)$.
    \end{enumerate}
    \item Montrer que les coefficients de Fourier $c_n(f)$ d'une solution $2\pi$-périodique de l'équation $(E_{a,b})$ vérifient la relation :
    \[
      \forall n\in\mathbb{Z},\qquad (n^2-a)c_n(f)=bc_{n-2}(f).
    \]
    \item Soit $\suiteN{\gamma_n}$ une suite de nombres complexes définie par :
    \[
      \begin{cases}
        \gamma_0=1                &                                      \\
        \forall n\in\mathbb{N}^*, & \gamma_n=\frac{b\gamma_{n-1}}{4n^2},
      \end{cases}
    \]
    et $\varphi$ la fonction définie sur $\mathbb{R}$ par $\varphi(t)=\sum_{n=0}^{+\infty}\gamma_n e^{2int}$. Montrer que la fonction $\varphi$ est une solution $2\pi$-périodique de l'équation $(E_{0,b})$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Soient $f,g:\mathbb{R}\longrightarrow \mathbb{C}$ deux fonctions continues, $2\pi$-périodiques. On note $f\ast g$ la fonction définie par
  \[
    (f\ast g)(x)=\frac{1}{2\pi}\int_0^{2\pi}f(t)g(x-t)\,dt,\qquad x\in\mathbb{R}.
  \]
  \begin{enumerate}
  \item Montrer que $f\ast g$ est une fonction $2\pi$-périodique, continue sur $\mathbb{R}$.
  \item Pour $n\in\mathbb{Z}$, calculer $\widehat{f\ast g}(n):=c_n(f\ast g)$ en fonction de $\hat f(n)$ et $\hat g(n)$.
  \item Démontrer que la série de Fourier de $f\ast g$ converge normalement sur $\mathbb{R}$ et calculer sa somme.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Soit $f$ une fonction $2\pi$-périodique et continue sur $\mathbb{R}$. Notons $S_n(f)$ la $n$-ème somme partielle de la série de Fourier de $f$, c'est-à-dire $S_n(f)(t)=\sum_{|k|\leq n}c_k(f)e^{ikt}$, $t\in\mathbb{R}$.  Supposons que pour tout $n\in\mathbb{N}$, on a $\|S_n(f)\|_\infty\leq 1$. Montrer alors que $\|f\|_\infty\leq 1$.
\end{exo}

% -----------------------------------------------
\begin{exo}

  Soit $f$ une fonction $2\pi$-périodique et continue de $\mathbb{R}$ dans $\mathbb{R}$. Montrer que $f$ est de classe $C^\infty$ sur $\mathbb{R}$ si et seulement si pour tout $k\in\mathbb{N}$, on a $c_n(f)=o(n^{-k})$, $|n|\to+\infty$.
\end{exo}

% -----------------------------------------------
\begin{exo}

  Soit $f:\mathbb{R}\longrightarrow\mathbb{C}$ une fonction $2\pi$-périodique et supposons qu'il existe $\alpha>0$ et $C>0$ tel que pour tout $x,y\in\mathbb{R}$, on a
  \[
    |f(x)-f(y)|\leq C|x-y|^\alpha.
  \]
  \begin{enumerate}
    \item Montrer que $f$ est uniformément continue sur $\mathbb{R}$.
    \item Pour $a\in\mathbb{R}$ et $n\in\mathbb{Z}$, exprimer
    \[
      \frac{1}{2\pi} \int_{-\pi}^\pi f(t+a)e^{-int}\,dt
    \]
    en fonction du coefficient de Fourier $c_n(f)$ de $f$.
    \item Soit $g_a(t)=f(t+a)-f(t-a)$, $t\in\mathbb{R}$. Pour $n\in\mathbb{Z}$, exprimer le coefficient de Fourier $c_n(g_a)$ en fonction de $c_n(f)$.
    \item En déduire que
    \[
      \sum_{n\in\mathbb{Z}}|\sin(na)|^2 |c_n(f)|^2\leq C^2 4^{\alpha-1}a^{2\alpha}.
    \]
    \item On suppose maintenant que $\alpha>1/2$.
    \begin{enumerate}
      \item Fixons $p\in\mathbb{N}^*$. Montrer que
      \[
        \sum_{2^{p-1}\leq |n|<2^p}\left|\sin\left(n\frac{\pi}{2^{p+1}}\right)\right|^2 |c_n(f)|^2\leq C^2 \frac{\pi^{2\alpha}}{4^{\alpha p+1}}.
      \]
      \item En déduire que
      \[
        \sum_{2^{p-1}\leq |n|<2^p}|c_n(f)|\leq \frac{C}{\sqrt 2}\frac{\pi^\alpha}{2^{p(\alpha-1/2)}}.
      \]
      \item Conclure que la série de Fourier de $f$ converge normalement vers $f$ sur $\mathbb{R}$.
    \end{enumerate}
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  \begin{enumerate}
    \item Soit $\suite[n\geq 1]{u_n}$ une suite réelle et $\ell\in\mathbb{R}\cup\{+\infty,-\infty\}$. Supposons que $\suite[n\geq 1]{u_n}$ tend vers $\ell$. Montrer alors que si
    \[
      c_n=\frac{1}{n}\sum_{k=1}^n u_k,\qquad n\geq 1,
    \]
    alors la suite $\suite[n\geq 1]{c_n}$ tend aussi vers $\ell$.
    \item Montrer que si $\suite[n\geq 1]{u_n}$ est une suite monotone, alors la réciproque est aussi vraie.
    \item En déduire que si $f$ est une fonction continue et $2\pi$ périodique sur $\mathbb{R}$ et telle que pour tout $n\in\mathbb{Z}$, $c_n(f)\geq 0$, alors la série de Fourier de $f$ converge normalement vers $f$ sur $\mathbb{R}$.\\
    \begin{indication}
      On pourra considérer les sommes de Féjer en $0$.
    \end{indication}
  \end{enumerate}
\end{exo}

\end{document}
