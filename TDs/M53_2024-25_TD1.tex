\documentclass[a4paper,10pt,reqno]{amsart}
\usepackage{lille}
\input{m53}

\lilleset{titre=TD1 - Rappels}

\begin{document}


% ===============================================
\section{Uniforme continuité}
% ===============================================

% -----------------------------------------------
\begin{exo}
  \begin{enumerate}
    \item Montrer que si $f:I\longrightarrow \mathbb R$ est une fonction $k$-lipschitzienne sur un intervalle $I$ (c'est-à-dire $\forall x,y\in I$, $|f(x)-f(y)|\leq k |x-y|$), alors $f$ est uniformément continue sur $I$.
    \item En déduire que $x\longmapsto \sin(x)$ est uniformément continue sur $\mathbb R$.
    \item Montrer que $x\longmapsto \dfrac{1}{1+|x|}$ est uniformément continue sur $\mathbb R$.
    \item Montrer que $x\longmapsto \sqrt{x}$ est uniformément continue sur $\mathbb R_+$ mais n'est pas lipschtzienne sur $\mathbb R_+$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Montrer que $x\longmapsto x^2$ est uniformément continue sur tout compact de $\mathbb R$ mais n'est pas uniformément continue sur $\mathbb R$.
\end{exo}

% -----------------------------------------------
\begin{exo}

  Soit $f:\mathbb R_+\longrightarrow\mathbb R$ une fonction uniformément continue. Le but de l'exercice est de montrer qu'il existe $a,b\in\mathbb R$ tels que $\forall x\geq 0$, $f(x)\leq ax+b$ (autrement dit, une fonction uniformément continue sur $\mathbb R_+$ est majorée par une fonction affine).
  \begin{enumerate}
    \item Justifier l'existence de $\eta_1>0$ tel que
    \[
      |x-y|<\eta_1\implies |f(x)-f(y)|\leq 1.
    \]
    \item Soit $x_0\in\mathbb R_+$ et $n_0=\left[\frac{x_0}{\eta_1}\right]+1$ (où $[\cdot]$ désigne la partie entière).
    \begin{enumerate}
      \item Montrer que
      \[
        |f(x_0)-f(0)|\leq \sum_{k=0}^{n_0-1}\left|f\left(\frac{(k+1)x_0}{n_0}\right)-f\left(\frac{k x_0}{n_0}\right)\right|.
      \]
      \item En déduire le résultat.
    \end{enumerate}
    \item{\bf{Application :}}
    \begin{enumerate}
      \item Montrer que $f:x\longmapsto e^x$ n'est pas uniformément continue sur $\mathbb R_+$.
      \item Soit $P$ un polynôme à coefficients réels. Montrer que $P$ est uniformément continue sur $\mathbb R$ si et seulement si $P$ est un polynôme de degré inférieur ou égal à $1$.
    \end{enumerate}
  \end{enumerate}
\end{exo}

\newpage
% ===============================================
\section{Intégrales de Riemann}
% ===============================================

% -----------------------------------------------
\begin{exo}

  Soit $f:I\longrightarrow\mathbb R$ une fonction définie et continue sur un intervalle (quelconque) $I$ de $\mathbb R$ et soit $a\in I$. On définit alors pour $x\in I$
  \[
    F(x)=\int_a^x f(t)\,dt.
  \]
  Montrer que $F$ est dérivable sur $I$ et que pour tout $x\in I$, on a $F'(x)=f(x)$.
\end{exo}

% -----------------------------------------------
\begin{exo}

  Soit $f$ la fonction définie par
  $\displaystyle
    f(x)=\int_x^{2x}\dfrac{e^{-t}}{t}\,dt.
  $
  \begin{enumerate}
    \item Montrer que $f$ est définie, continue et dérivable sur $\mathbb R^*$ et calculer sa dérivée.
    \item Montrer que pour tout $x>0$, $e^{-2x}\ln(2)\leq f(x)\leq e^{-x}\ln(2)$. Etablir une inégalité analogue sur $\mathbb R_-^*$.
    \item En déduire que $f$ peut se prolonger par continuité en $0$.
    \item Etudier les limites de $f$ à l'infini.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}[.7]

  Pour $n\in\mathbb N$, on pose
  $\displaystyle
    I_n=\int_{0}^{\frac{\pi}{2}}\sin^n(x)\,dx.
  $
  Montrer que $\lim_{n\to +\infty}I_n=0$.\\
  \begin{indication}
    Soit $\varepsilon>0$, on pourra découper l'intégrale sur $[0,\pi/2-\varepsilon]$ et $[\pi/2-\varepsilon,\pi/2]$.
  \end{indication}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Le but de l'exercice est de montrer le résultat suivant connu sous le nom de {\emph{deuxième formule de la moyenne :}} soient $f,g$ deux fonctions définies et continues de $[a,b]$ dans $\mathbb R$. On suppose de plus que $g$ est positive et décroissante. Alors il existe $c\in [a,b]$ tel que
  \begin{equation}
    \int_a^b f(x)g(x)\,dx=g(a)\int_a^c f(x)\,dx.
  \end{equation}
  \begin{enumerate}
    \item Montrer d'abord le cas \enquote{facile}, quand $f$ est positive.
  \end{enumerate}
  Pour montrer le cas général, considérons $\mathcal S=(x_0,x_1,\dots x_n)$ une subdivision de $[a,b]$, c'est-à-dire $x_0=a<x_1<\dots<x_{n-1}<x_n=b$, et notons $\delta(\mathcal S)=\max_{1\leq i \leq n}(x_i-x_{i-1})$ le pas de la subdivision. On notera aussi $\|f\|_\infty=\sup_{x\in [a,b]}|f(x)|$ et $F(x)=\int_a^x f(t)\,dt$, $x\in [a,b]$.
  \begin{enumerate}[resume]
    \item Montrer que
    $
    \left|\sum_{i=1}^n \int_{x_{i-1}}^{x_i}(g(x)-g(x_{i-1}))f(x)\,dx \right|\leq \|f\|_\infty\left(\sum_{i=1}^n (x_i-x_{i-1})g(x_{i-1}) -\int_a^b g(x)\,dx\right).
    $
    \item En déduire que
    \[
      \int_a ^b f(x)g(x)\,dx=\lim_{\delta(\mathcal S)\to 0}\sum_{i=1}^n g(x_{i-1})\int_{x_{i-1}}^{x_i}f(x)\,dx.
    \]
    \item Justifier que $F$  admet un minimum $m$ et un maximum $M$ sur $[a,b]$.
    \item En utilisant une transformation d'Abel, montrer que
    \[
      m g(a)\leq \sum_{i=1}^n g(x_{i-1})\int_{x_{i-1}}^{x_i}f(x)\,dx \leq M g(a).
    \]
    \item En déduire le résultat.
  \end{enumerate}

  \textbf{Application :} Soit $f$ une application continue décroissante de $[0,+\infty[$ dans $\mathbb R$ qui tend vers zéro à l'infini. Calculer
  \[
    \lim_{n\to +\infty}\int_n^{n^2}f(t)e^{it}\,dt.
  \]
\end{exo}

% ===============================================
\section{Intégrales généralisées}
% ===============================================

% -----------------------------------------------
\begin{exo}

  Les intégrales impropres suivantes sont-elles convergentes ?
  \begin{center}
    \begin{enumerate*}[\quad\bf a)]
      \item $\displaystyle \int_0^1 \ln tdt,$
      \item $\displaystyle \int_0^{+\infty}e^{-t^2}dt,$
      \item $\displaystyle \int_0^{+\infty}x(\sin x)e^{-x}dx,$
      \item $\displaystyle \int_0^{+\infty}(\ln t)e^{-t}dt,$
      \item $\displaystyle \int_0^1 \frac{dt}{(1-t)\sqrt t}.$
    \end{enumerate*}
  \end{center}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Les intégrales impropres suivantes sont-elles convergentes ?
  \begin{center}
    \begin{enumerate*}[\quad\bf a)]
      \item $\displaystyle \int_0^{+\infty}\frac{dt}{e^t-1},$
      \item $\displaystyle \int_0^{+\infty}\frac{te^{-\sqrt t}}{1+t^2}dt,$
      \item $\displaystyle \int_0^1 \cos^2\left(\frac1t\right)dt.$
    \end{enumerate*}
  \end{center}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Les intégrales impropres suivantes sont-elles convergentes ?
  \begin{center}
    \begin{enumerate*}[\quad\bf a)]
      \item $\displaystyle \int_0^{+\infty}\frac{\ln t}{t^2+1}dt,$
      \item $\displaystyle \int_1^{+\infty}\frac{\sqrt{\ln x}}{(x-1)\sqrt x}dx,$
      \item $\displaystyle \int_1^{+\infty} e^{-\sqrt{\ln t}}dt.$
    \end{enumerate*}
  \end{center}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Discuter, suivant la valeur du paramètre $\alpha\in\mathbb R$, la convergence des intégrales impropres suivantes :
  \begin{center}
    \begin{enumerate*}[\quad\bf a)]
      \item $\displaystyle \int_0^{+\infty}\frac{e^{-t}-1}{t^\alpha}dt,$
      \item $\displaystyle \int_0^{+\infty}\frac{t-\sin t}{t^\alpha}dt,$
      \item $\displaystyle \int_0^{+\infty}\frac{\arctan t}{t^\alpha}dt.$
    \end{enumerate*}
  \end{center}
\end{exo}

% -----------------------------------------------
\begin{exo}
  \begin{enumerate}
    \item Démontrer la convergence de l'intégrale
      $\displaystyle \int_0^1 \frac{\ln x}{x^{3/4}}dx$.
    \item Démontrer la convergence de
      $\displaystyle \int_0^1\frac{\ln\left(x+\sqrt x\right)-\ln(x)}{x^{3/4}}dx$.
    \item Etudier la nature de
      $\displaystyle \int_1^{+\infty}\frac{\ln\left(x+\sqrt x\right)-\ln(x)}{x^{3/4}}dx$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}
  \begin{enumerate}
    \item Soit $f:[0,+\infty[\to\mathbb R$ une fonction continue. On suppose que $\int_0^{+\infty}f(t)dt$ converge, et soit $\suite{x_n}$ et $\suite{y_n}$ deux suites tendant vers $+\infty$. Démontrer que $\int_{x_n}^{y_n}f(t)dt$ tend vers 0.
    \item En déduire que l'intégrale $\int_0^{+\infty}e^{-t\sin t}dt$ diverge.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}
  \begin{enumerate}
    \item Soient $f$ et $g$ deux fonctions définies et continues sur un intervalle $[a,+\infty[$ vérifiant les conditions suivantes :
    \begin{enumerate}[(i)]
      \item il existe un réel $M$ tel que pour tout $x\geq a$, on a
      \[
        \left|\int_a^x f(t)\,dt\right|\leq M;
      \]
      \item la fonction $g$ est décroissante et $\displaystyle\lim_{t\to +\infty}g(t)=0$.
    \end{enumerate}
    Montrer que l'intégrale $\int_a^{+\infty}f(t)g(t)\,dt$ est convergente.\\
    \begin{indication}
      on pourra utiliser la deuxième formule de la moyenne.
    \end{indication}
    \item En déduire que l'intégrale $\int_1^{\infty}\frac{\sin(t)}{t^\alpha}\,dt$ est convergente, pour $\alpha > 0$.
    \item Donner une autre preuve de la convergence de l'intégrale précédente en utilisant une intégration par partie.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Étudier la convergence des intégrales suivantes :
  \begin{center}
    \begin{enumerate*}[\quad\bf a)]
      \item $\displaystyle \int_4^{+\infty}\frac{\sin x}{\sqrt x+\sin x }dx,$
      \item $\displaystyle \int_1^{+\infty}\ln\left(1+\frac{\sin x}{x^\alpha}\right)dx,\ \alpha>0.$
    \end{enumerate*}
  \end{center}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Justifier la convergence et calculer la valeur des intégrales suivantes :
  \begin{center}
    \begin{enumerate*}[\quad\bf a)]
      \item $\displaystyle \int_0^{1}\frac{\ln t}{\sqrt{1-t}}dt,$
      \item $\displaystyle \int_0^{+\infty}te^{-\sqrt t}dt,$
      \item $\displaystyle \int_0^{+\infty}\sin(t)e^{-at}dt,\ a>0.$
    \end{enumerate*}
  \end{center}
\end{exo}

% -----------------------------------------------
\begin{exo}[.7]

  Discuter suivant la valeur du paramètre $\alpha,\beta\in\mathbb R$ la convergence de l'intégrale
  \[
    \int_e^{+\infty}\frac{dt}{t^\alpha(\ln t)^\beta}.
  \]
\end{exo}

\end{document}
