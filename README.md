# [m53lille](https://m53lille.gitlabpages.univ-lille.fr/) [<img src="https://about.gitlab.com/images/press/logo/svg/gitlab-logo-500.svg" height="50" style="vertical-align: middle">](https://gitlab.univ-lille.fr/m53lille/m53lille.gitlabpages.univ-lille.fr/)

Les documents du module M53 - « Intégrales à paramètres et séries de Fourier » de la L3 Mathématiques à l'Université de Lille.

## 2024/25

Vous pouvez obtenir [ce dépôt](https://gitlab.univ-lille.fr/m53lille/m53lille.gitlabpages.univ-lille.fr/) de deux façons faciles :

- en téléchargeant le [zip](https://gitlab.univ-lille.fr/m53lille/m53lille.gitlabpages.univ-lille.fr/-/archive/main/m53lille.gitlabpages.univ-lille.fr-main.zip) qui contient la dernière version des fichiers,
- en clonant le dépôt entier, l'historique y compris, en utilisant la commande `git` suivante

  ```shell
  git clone https://gitlab.univ-lille.fr/m53lille/m53lille.gitlabpages.univ-lille.fr.git .
  ```

Dans [ce dépôt](https://gitlab.univ-lille.fr/m53lille/m53lille.gitlabpages.univ-lille.fr/) vous pouvez trouver les sources LaTeX et les PDFs des documents suivants :

### Les chapitres du cours
- Chapitre 1 **[[pdf](Cours/Chap1-intégrales_généralisées.pdf)]** [[tex](Cours/Chap1-intégrales_généralisées.tex)]
- Chapitre 2 **[[pdf](Cours/Chap2-intégrales_à_paramètres_intervalles_bornes.pdf)]** [[tex](Cours/Chap2-intégrales_à_paramètres_intervalles_bornes.tex)]
- Chapitre 3 **[[pdf](Cours/Chap3-intégrales_à_paramètres_impropres.pdf)]** [[tex](Cours/Chap3-intégrales_à_paramètres_impropres.tex)]
- Chapitre 4 **[[pdf](Cours/Chap4-séries_de_Fourier.pdf)]** [[tex](Cours/Chap4-séries_de_Fourier.tex)]


### Les feuilles de td _(compilés avec [tectonic](https://tectonic-typesetting.github.io))_
- TD n°1 **[[pdf](TDs/M53_2024-25_TD1.pdf)]** [[tex](TDs/M53_2024-25_TD1.tex)]
- TD n°2 **[[pdf](TDs/M53_2024-25_TD2.pdf)]** [[tex](TDs/M53_2024-25_TD2.tex)]
- TD n°3 **[[pdf](TDs/M53_2024-25_TD3.pdf)]** [[tex](TDs/M53_2024-25_TD3.tex)]
- TD n°4 **[[pdf](TDs/M53_2024-25_TD4.pdf)]** [[tex](TDs/M53_2024-25_TD4.tex)]

### Les examens

- DS1 2024/25 **[[sujet](Examens/M53_2024-25_DS1_sujet.pdf)]** **[[solutiuons](Examens/M53_2024-25_DS1_solutions.pdf)]** [[tex](Examens/M53_2024-25_DS1.tex)]
- DS2 2024/25 **[[sujet](Examens/M53_2024-25_DS2_sujet.pdf)]** **[[solutiuons](Examens/M53_2024-25_DS2_solutions.pdf)]** [[tex](Examens/M53_2024-25_DS2.tex)]

### Fichiers complémentaires
Pour compiler ces fichiers vous avez besoin des styles et des logos suivants :

- pour les feuilles de td
  - [lille.sty](TDs/lille.sty)
  - [m53.tex](TDs/m53.tex)
  - [lille.pdf](TDs/lille.pdf)
- pour les chapitres du cours
  - [cours.cls](Cours/cours.cls)
  - [macro.tex](Cours/macro.tex)

## 2023/24

Vous pouvez trouver les sources LaTeX et les PDFs de 2023, ainsi que les annales des années précédentes, à l'adresse suivante :

https://m53lille.gitlabpages.univ-lille.fr/2023/

---
[Licence MIT](LICENSE)


